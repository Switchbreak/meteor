package  
{
	import flash.utils.Dictionary;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Draw;
	
	/**
	 * Helper class to fire events at game object targets
	 * 
	 * @author Switchbreak
	 */
	public class EventTargeter 
	{
		// Constants
		private static const EVENT_LINE_COLOR:uint = 0x8888FF;
		private static const EVENT_LINE_ALPHA:Number = 0.5;
		
		// Private variables
		public var events:Dictionary;
		
		/**
		 * Create and initialize the event targeter
		 */
		public function EventTargeter( objectXML:XML = null ) 
		{
			events = new Dictionary();
			
			// Load all events from object XML
			if ( objectXML != null )
			{
				for each( var node:XML in objectXML.event )
				{
					for each( var targetNode:XML in node.target )
						AddTarget( node.@name, targetNode.@name, targetNode.@event );
				}
			}
		}
		
		/**
		 * Fire off an event by triggering every target listed for that event
		 * 
		 * @param	eventName
		 * Name of the event to fire
		 * @param	value
		 * Optional value to send as an event parameter
		 */
		public function FireEvent( eventName:String, value:int = 0 ):void
		{
			// Loop through all targets under the fired event name
			for each( var target:EventTarget in events[eventName] )
			{
				GameWorld.TriggerEvent( target.targetName, target.targetEvent, value );
			}
		}
		
		/**
		 * Convert event targeter into a savable XML object and append it to the
		 * parent object's XML object
		 * 
		 * @param	node
		 * Parent object, serialized
		 */
		public function Serialize( node:XML ):void
		{
			for( var eventName:String in events )
			{
				var eventNode:XML = <event name={eventName} />
				for each( var target:EventTarget in events[eventName] )
				{
					eventNode.appendChild( <target name={target.targetName} event={target.targetEvent} /> );
				}
				
				node.appendChild( eventNode );
			}
		}
		
		/**
		 * Gets the list of target names on an event
		 * 
		 * @param	eventName
		 * Name of the event to retrieve
		 * @return
		 * Vector containing target names
		 */
		public function GetTargetList( eventName:String ):Vector.<String>
		{
			var targets:Vector.<EventTarget> = events[eventName];
			var targetNames:Vector.<String> = new Vector.<String>();
			
			// If there are targets on the specified event, build a list of them
			if ( targets != null )
			{
				for each( var target:EventTarget in targets )
				{
					targetNames.push( target.targetName );
				}
			}
			
			return targetNames;
		}
		
		/**
		 * Adds a new target to a specified event
		 * 
		 * @param	eventName
		 * Name of the event to add the target to
		 * @param	targetName
		 * Name of the target to trigger when the event fires
		 * @param	targetEvent
		 * Name of the event on the target object to trigger
		 */
		public function AddTarget( eventName:String, targetName:String, targetEvent:String ):void
		{
			var target:EventTarget = new EventTarget();
			target.targetName = targetName;
			target.targetEvent = targetEvent;
			
			if ( events[eventName] == null )
				events[eventName] = new Vector.<EventTarget>();
			events[eventName].push( target );
		}
		
		/**
		 * Edits an existing target on a specified event
		 * 
		 * @param	eventName
		 * Name of the event on which the target lives
		 * @param	targetIndex
		 * Index of the target on the event
		 * @param	targetName
		 * Edited name of the target
		 * @param	targetEvent
		 * Edited name of the target event
		 */
		public function EditTarget( eventName:String, targetIndex:int, targetName:String, targetEvent:String ):void
		{
			if ( events[eventName] != null && events[eventName].length > targetIndex )
			{
				events[eventName][targetIndex].targetName = targetName;
				events[eventName][targetIndex].targetEvent = targetEvent;
			}
		}
		
		/**
		 * Edits an existing target on a specified event
		 * 
		 * @param	eventName
		 * Name of the event on which the target lives
		 * @param	targetIndex
		 * Index of the target on the event
		 * @param	targetName
		 * Edited name of the target
		 * @param	targetEvent
		 * Edited name of the target event
		 */
		public function DeleteTarget( eventName:String, targetIndex:int ):void
		{
			if ( events[eventName] != null && events[eventName].length > targetIndex )
			{
				events[eventName].splice( targetIndex, 1 );
			}
		}
		
		/**
		 * Gets the target name and target event name of a specified target
		 * 
		 * @param	eventName
		 * Event to pull the target from
		 * @param	targetIndex
		 * Index of the target on the event
		 * @return
		 * Array with the following values
		 * 0 - Target name
		 * 1 - Target event name
		 */
		public function ReturnTarget( eventName:String, targetIndex:int ):Vector.<String>
		{
			if ( events[eventName] == null || events[eventName].length <= targetIndex )
				return null;
			
			var returnValue:Vector.<String> = new Vector.<String>();
			returnValue[0] = events[eventName][targetIndex].targetName;
			returnValue[1] = events[eventName][targetIndex].targetEvent;
			
			return returnValue;
		}
		
		/**
		 * Draw lines from a start point to all the target game object positions
		 * 
		 * @param	startX
		 * @param	startY
		 */
		public function DrawEventLines( startX:int, startY:int ):void
		{
			// Loop through all events and targets
			for( var eventName:String in events )
			{
				for each( var target:EventTarget in events[eventName] )
				{
					// Check if the target is an existing game object
					var object:Entity = FP.world.getInstance( target.targetName );
					if ( object != null )
					{
						// Draw the line to the object's center
						Draw.linePlus( startX, startY, object.x + object.halfWidth, object.y + object.halfHeight, EVENT_LINE_COLOR, EVENT_LINE_ALPHA );
					}
				}
			}
		}
	}
}
	
/**
 * Class to store event target information
 * 
 * @author Switchbreak
 */
class EventTarget 
{
	public var targetName:String;
	public var targetEvent:String;
}