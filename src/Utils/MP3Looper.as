package Utils
{
	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.SampleDataEvent;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFormat;
	import flash.utils.ByteArray;

	/**
	 * Gapless-looping MP3 player class, based on the code here:
	 * 
	 * http://blog.andre-michelle.com/2010/playback-mp3-loop-gapless/
	 *
	 *** Original notes:
	 * Tested with samplingrate 44.1 KHz
	 *
	 * <code>MAGIC_DELAY</code> does not change on different bitrates.
	 * Value was found by comparision of encoded and original audio file.
	 *
	 *** Usage:
	 * This class is designed to work much like FlashPunk's Sfx class; the major
	 * difference is that when you instanciate it, you have to specify the length
	 * of the original WAV file, in number of samples (eg: 44100 per second). 
	 * eg: for a sound exactly two seconds long:
	 * 
	 * [Embed(source = 'assets/noise.mp3')]
     * private const NOISE_SFX:Class;
     * private var noiseLoop:MP3Looper = new MP3Looper(NOISE_SFX, 88200);
	 *
	 * Then start and stop playback with the .play() and .stop() methods.
	 * .pan and .volume properties are available to alter the playback volume and panning
	 *
	 */
	public final class MP3Looper
	{
		private const MAGIC_DELAY:Number = 2257.0; // LAME 3.98.2 + flash.media.Sound Delay

		private const bufferSize: int = 8192; // Stable playback
		private var samplesTotal:int; // original amount of sample before encoding (change it to your loop)

		private var mp3:Sound;
		private const out: Sound = new Sound(); // Use for output stream
        private var channel:SoundChannel;
        private var myTransform:SoundTransform = new SoundTransform(1, 0);

		private var samplesPosition: int = 0;

		private var enabled: Boolean = false;

		public function MP3Looper(mp3asset:*, numSamples:int)
		{
		    mp3 = new mp3asset() as Sound;
		    samplesTotal = numSamples;
			//startPlayback();
			out.addEventListener( SampleDataEvent.SAMPLE_DATA, sampleData );
		}

        public function get playing():Boolean{ return enabled; }

		public function loop():void
		{
            if(!enabled) {
                enabled = true;
                
                samplesPosition = 0;
                //out.addEventListener( SampleDataEvent.SAMPLE_DATA, sampleData );
                
                channel = out.play(0, 0, myTransform);
		    }
		}
		
		public function stop():void
		{
		    if(enabled) {
                enabled = false;
                channel.stop();
            }
		}
		
		public function get pan():Number { return myTransform.pan; }
        public function set pan(newpan:Number):void
        {
            myTransform.pan = newpan;
            if(channel) {
                channel.soundTransform = myTransform;
            }
        }
        
        public function get volume():Number { return myTransform.volume; }
        public function set volume(newvolume:Number):void
        {
            myTransform.volume = newvolume;
            if(channel) {
                channel.soundTransform = myTransform;
            }
        }
        
		private function sampleData( event:SampleDataEvent ):void
		{
			if( enabled )
			{
				extract( event.data, bufferSize );
			}
			else
			{
				silent( event.data, bufferSize );
			}
		}

		/**
		 * This methods extracts audio data from the mp3 and wraps it automatically with respect to encoder delay
		 *
		 * @param target The ByteArray where to write the audio data
		 * @param length The amount of samples to be read
		 */
		private function extract( target: ByteArray, length:int ):void
		{
			while( 0 < length )
			{
				if( samplesPosition + length > samplesTotal )
				{
					var read: int = samplesTotal - samplesPosition;

					mp3.extract( target, read, samplesPosition + MAGIC_DELAY );

					samplesPosition += read;

					length -= read;
				}
				else
				{
					mp3.extract( target, length, samplesPosition + MAGIC_DELAY );

					samplesPosition += length;

					length = 0;
				}

				if( samplesPosition == samplesTotal ) // END OF LOOP > WRAP
				{
					samplesPosition = 0;
				}
			}
		}

		private function silent( target:ByteArray, length:int ):void
		{
			target.position = 0;

			while( length-- )
			{
				target.writeFloat( 0.0 );
				target.writeFloat( 0.0 );
			}
		}
	}
}