package Utils 
{
	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.events.Event;
	import flash.utils.ByteArray;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Stamp;
	
	/**
	 * Utility class to handle asynchronous loading of images from ByteArrays
	 * 
	 * @author Switchbreak
	 */
	public class ImageLoader 
	{
		// Private variables
		private var target:Entity;
		
		/**
		 * Instantiate asynchronous image loader, set target entity graphic to a
		 * Stamp object containing the image.
		 * 
		 * @param	setTarget
		 * Entity to load the image into
		 * @param	imageData
		 * Image binary data
		 */
		public function ImageLoader( setTarget:Entity, imageData:ByteArray ) 
		{
			// Set object values
			target = setTarget;
			
			// Load image
			var loader:Loader = new Loader();
			loader.contentLoaderInfo.addEventListener( Event.COMPLETE, OnComplete );
			loader.loadBytes( imageData );
		}
		
		/**
		 * Image has completed loading
		 * 
		 * @param	evt
		 * Event handle
		 */
		private function OnComplete( evt:Event ):void
		{
			// Add loaded image
			var loadedBitmap:Bitmap = evt.target.loader.content as Bitmap;
			target.graphic = new Stamp( loadedBitmap.bitmapData );
			target.setHitboxTo( target.graphic );
			
			evt.target.removeEventListener( Event.COMPLETE, OnComplete );
		}
	}

}