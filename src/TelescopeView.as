package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.FP;
	import net.flashpunk.tweens.misc.Alarm;
	
	/**
	 * Telescope look cutscene
	 * @author Switchbreak
	 */
	public class TelescopeView extends Entity 
	{
		// Embeds
		[Embed(source = "../img/Telescope.png")] private static const TELESCOPE:Class;
		
		// Constants
		private static const SCENE_DURATION:Number = 1.0;
		
		/**
		 * Create and initialize the object
		 */
		public function TelescopeView() 
		{
			// Show image
			super( 0, 0, new Stamp( TELESCOPE ) );
			layer = GameWorld.HUD_LAYER;
			
			// Deactivate player input
			GameWorld.currentInstance.player.active = false;
			
			// Set timer to close the scene
			addTween( new Alarm( SCENE_DURATION, CloseView, ONESHOT ), true );
		}
		
		/**
		 * Reactivate player input and remove the telescope view
		 */
		private function CloseView():void
		{
			GameWorld.currentInstance.player.active = true;
			FP.world.remove( this );
		}
		
	}

}