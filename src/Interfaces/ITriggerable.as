package Interfaces 
{
	
	/**
	 * Interface for triggerable objects
	 * 
	 * @author Switchbreak
	 */
	public interface ITriggerable 
	{
		/**
		 * Trigger the object
		 */
		function Trigger( event:String, value:int ):void;
		
		/**
		 * Get list of trigger events for editor
		 */
		function get triggers():Vector.<String>;
	}
	
}