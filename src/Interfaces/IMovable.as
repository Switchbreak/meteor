package Interfaces 
{
	import flash.geom.Point;
	
	/**
	 * Interface for movable objects
	 * 
	 * @author Switchbreak
	 */
	public interface IMovable 
	{
		/**
		 * Move to a destination point immediately
		 * 
		 * @param	destPoint
		 * Position to warp to
		 */
		function WarpTo( destPoint:Point ):void;
		
		/**
		 * Move to a destination point at your own speed
		 * 
		 * @param	destPoint
		 * Position to move to
		 * @param	completeCallback
		 * Function to call when the move is complete
		 */
		function MoveTo( destPoint:Point, completeCallback:Function ):void;
	}
	
}