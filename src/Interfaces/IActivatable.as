package Interfaces
{
	import net.flashpunk.Entity;
	
	/**
	 * Interface for all game objects that activate when the player enters their
	 * hitbox
	 * 
	 * @author Switchbreak
	 */
	public interface IActivatable
	{
		/**
		 * Get or set the state of an activated object
		 */
		function get activated():Boolean;
		function set activated( value:Boolean ):void;
	}
	
}