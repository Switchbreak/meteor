package Interfaces
{
	
	/**
	 * Interface for game objects that are savable
	 * 
	 * @author Switchbreak
	 */
	public interface ISavable 
	{
		/**
		 * Get XML object of savable object
		 * 
		 * @return
		 * Serialized game object
		 */
		function GetSaveObject():XML;
		
		/**
		 * Get the list of properties on the game object
		 * 
		 * @return
		 * Array of strings containing the names of properties
		 */
		function GetProperties():Vector.<String>;
	}
	
}