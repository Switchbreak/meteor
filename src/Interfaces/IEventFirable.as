package Interfaces 
{
	
	/**
	 * Interface for triggerable objects
	 * 
	 * @author Switchbreak
	 */
	public interface IEventFirable 
	{
		/**
		 * Get list of trigger events for editor
		 */
		function get events():Vector.<String>;
		
		/**
		 * Get event targeter
		 */
		function get target():EventTargeter;
	}

	
}