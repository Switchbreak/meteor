package Interfaces 
{
	
	/**
	 * Interface for game objects with edit handles
	 * 
	 * @author Switchbreak
	 */
	public interface IEditHandle 
	{
		/**
		 * Show the object's edit handle
		 */
		function ShowHandle():void;
		
		/**
		 * Hide the object's edit handle
		 */
		function HideHandle():void;
	}
	
}