package Interfaces 
{
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public interface ICollisionCircle extends IActivatable
	{
		function get radius():Number;
	}
	
}