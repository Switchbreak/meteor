package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import mx.resources.ResourceManager;
	import net.flashpunk.World;
	import net.flashpunk.utils.Input;
	
	/**
	 * Title screen
	 * @author Switchbreak
	 */
	[ResourceBundle("resources")]
	public class TitleScreen extends World 
	{
		// Embeds
		[Embed(source = "../img/Title.png")] private static const TITLE_IMAGE:Class;
		
		// Constants
		private static const NEW_GAME_Y:int = 300;
		private static const CONTINUE_Y:int = 350;
		private static const DISABLED_COLOR:uint = 0x888888;
		private static const BUTTON_TYPE:String = "Button";
		private static const NEW_GAME_BUTTON:String = "NewGame";
		private static const CONTINUE_BUTTON:String = "Continue";
		
		// Graphics
		private var titleImage:Stamp;
		private var newGame:Text;
		private var continueGame:Text;
		
		/**
		 * Create and initialize the title screen
		 */
		public function TitleScreen() 
		{
			FP.screen.color = 0;
			
			titleImage =  new Stamp( TITLE_IMAGE );
			addGraphic( titleImage );
			
			newGame = new Text( ResourceManager.getInstance().getString( "resources", "NEW_GAME" ) );
			continueGame = new Text( ResourceManager.getInstance().getString( "resources", "CONTINUE" ) );
			
			var object:Entity = add( new Entity( FP.halfWidth - (newGame.width >> 1), NEW_GAME_Y, newGame ) );
			object.setHitboxTo( object.graphic );
			object.type = BUTTON_TYPE;
			object.name = NEW_GAME_BUTTON;
			
			object = add( new Entity( FP.halfWidth - (continueGame.width >> 1), CONTINUE_Y, continueGame ) );
			object.setHitboxTo( object.graphic );
			object.name = CONTINUE_BUTTON;
			
			if ( PlayerSession.CheckSession() )
			{
				object.type = BUTTON_TYPE;
			}
			else
			{
				continueGame.color = DISABLED_COLOR;
			}
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			if ( Input.mousePressed )
			{
				var object:Entity = collidePoint( BUTTON_TYPE, Input.mouseX, Input.mouseY );
				if ( object != null )
				{
					switch( object.name )
					{
						case NEW_GAME_BUTTON:
							FP.world = new GameWorld();
							break;
						case CONTINUE_BUTTON:
							FP.world = new GameWorld( true );
							break;
					}
				}
			}
			
			super.update();
		}
		
	}

}