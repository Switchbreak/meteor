package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.utils.Input;
	import net.flashpunk.FP;
	
	/**
	 * Level editor object for placing game objects
	 * 
	 * @author Switchbreak
	 */
	public class PlaceObject extends Entity 
	{
		// Constants
		private static const SNAP_RADIUS:int = 10;
		
		// Private variables
		private var gameObject:Entity;
		private var callback:Function;
		
		/**
		 * Create and initialize the editor object
		 * 
		 * @param	setGameObject
		 * Game object to place
		 * @param	placed
		 * Function to call when the object is placed
		 */
		public function PlaceObject( setGameObject:Entity, placed:Function ) 
		{
			// Set object values
			gameObject = setGameObject;
			callback = placed;
		}
		
		/**
		 * Handle update
		 */
		override public function update():void 
		{
			// Set object position by mouse
			gameObject.x = (world.mouseX - gameObject.halfWidth);
			gameObject.y = (world.mouseY - gameObject.halfHeight);
			
			// Snap to edge of screen
			if ( gameObject.x - world.camera.x < SNAP_RADIUS && gameObject.x - world.camera.x > -SNAP_RADIUS )
			{
				gameObject.x = world.camera.x;
			}
			if ( gameObject.y - world.camera.y < SNAP_RADIUS && gameObject.y - world.camera.y > -SNAP_RADIUS )
			{
				gameObject.y = world.camera.y;
			}
			if ( (gameObject.x + gameObject.width) - (world.camera.x + FP.width) < SNAP_RADIUS && (gameObject.x + gameObject.width) - (world.camera.x + FP.width) > -SNAP_RADIUS )
			{
				gameObject.x = world.camera.x + FP.width - gameObject.width;
			}
			if ( (gameObject.y + gameObject.height) - (world.camera.y + FP.height) < SNAP_RADIUS && (gameObject.y + gameObject.height) - (world.camera.y + FP.height) > -SNAP_RADIUS )
			{
				gameObject.y = world.camera.y + FP.height - gameObject.height;
			}
			
			// Capture clicks
			if ( Input.mousePressed )
				callback();
			
			super.update();
		}
	}

}