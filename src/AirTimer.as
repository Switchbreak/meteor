package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import mx.resources.ResourceManager;
	import net.flashpunk.Sfx;
	import Utils.MP3Looper;
	
	/**
	 * Entity to display the remaining air
	 * 
	 * @author Switchbreak
	 */
	public class AirTimer extends Entity 
	{
	    // Embeds
	    [Embed(source = "../snd/suitair.mp3")] private static const SUIT_AIR_LOOP:Class;
	    [Embed(source = "../snd/inflate_tank.mp3")] private static const INFLATE_TANK_SND:Class;
	    
		// Public Constants
		public static const MAX_AIR:Number = 100;
		public static const RATE:Number = 2;
		public static const REFILL_RATE:Number = 5;
		
		// Constants
		private static const PADDING:int = 10;
		private static const BAR_WIDTH:int = 20;
		private static const BAR_COLOR:uint = 0xFFFFFF;
		private static const AIR_VOLUME:Number = 0.01;
		private static const INFLATE_TANK_VOLUME:Number = 0.1;
		
		// Private variables
		public var airLeft:Number = MAX_AIR;
		
		// Graphics
		private var airBar:Image;
		private var airText:Text;
		private var outdoor:Boolean;
		
		// Objects
		private var noiseLoop:MP3Looper;
		private var inflateTankSfx:Sfx = new Sfx( INFLATE_TANK_SND );
		
		/**
		 * Create and initialize the object
		 */
		public function AirTimer() 
		{
			// Create graphics
			airText = new Text( ResourceManager.getInstance().getString( "resources", "AIR_TEXT" ) );
			airText.width = FP.width;
			airText.align = "center";
			airText.y = PADDING;
			
			airBar = Image.createRect( FP.width - (PADDING << 1), BAR_WIDTH, BAR_COLOR );
			airBar.x = PADDING;
			airBar.y = airText.y + airText.height;
			
			// Create object
			super( 0, 0, new Graphiclist( airBar, airText ) );
			layer = GameWorld.HUD_LAYER;
			graphic.scrollX = 0;
			graphic.scrollY = 0;
			active = false;
			visible = false;
			
			// Create sound object
			noiseLoop = new MP3Looper(SUIT_AIR_LOOP, 127000)
			noiseLoop.volume = AIR_VOLUME;
		}
		
		/**
		 * Update remaining air
		 */
		override public function update():void 
		{
			// Decrement air
			if ( outdoor )
			{
				airLeft -= RATE * FP.elapsed;
				if ( airLeft < 0 )
				{
					airLeft = 0;
					noiseLoop.stop();
					active = false;
					GameWorld.TriggerEvent( "GameOver" );
				}
				//if(inflateTankSfx.playing) inflateTankSfx.stop();
			}
			else
			{
				airLeft += REFILL_RATE * FP.elapsed;
				if ( airLeft > MAX_AIR )
				{
					airLeft = MAX_AIR;
					
					active = false;
					visible = false;
				}
			}
			
			airBar.scaleX = airLeft / MAX_AIR;
			
			super.update();
		}
		
		/**
		 * Cleanup object when removed from the world
		 */
		override public function removed():void 
		{
			noiseLoop.stop();
			
			super.removed();
		}
		
		/**
		 * Call to switch air timer mode between outdoor (decreasing) and indoor
		 * (refilling) mode
		 * 
		 * @param	setOutdoor
		 * True for outdoor mode, false for indoor mode
		 */
		public function SetEnvironment( setOutdoor:Boolean ):void
		{
			outdoor = setOutdoor;
			noiseLoop.stop();
			
			if ( outdoor && airLeft > 0 )
			{
				// Show air bar and start decreasing it
				noiseLoop.loop();
				active = true;
				visible = true;
				if(inflateTankSfx.playing) inflateTankSfx.stop();
			}
			else if( !outdoor && airLeft < MAX_AIR )
			{
				if(!inflateTankSfx.playing)
				{
				    inflateTankSfx.play(INFLATE_TANK_VOLUME, 0, 1000.0* ((inflateTankSfx.length - 0.2) - (MAX_AIR - airLeft) / REFILL_RATE));
				}
				active = true;
				visible = true;
			}
		}
	}

}