package  
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.*;
	import Interfaces.*;
	
	/**
	 * Player object
	 * 
	 * @author Switchbreak
	 */
	public class Player extends Entity 
	{
		// Embeds
		[Embed(source = "../snd/footstep_outdoor.mp3")] private static const FOOTSTEP_OUTDOOR_SND:Class;
		[Embed(source = "../snd/footstep_outdoor2.mp3")] private static const FOOTSTEP_OUTDOOR_SND2:Class;
		[Embed(source = "../snd/footstep_indoor.mp3")]  private static const FOOTSTEP_INDOOR_SND:Class;
		[Embed(source = "../snd/footstep_indoor2.mp3")]  private static const FOOTSTEP_INDOOR_SND2:Class;
		[Embed(source = "../snd/footstep_indoor3.mp3")]  private static const FOOTSTEP_INDOOR_SND3:Class;
		[Embed(source = "../snd/airjump.mp3")] private static const AIR_JUMP_SND:Class;
		[Embed(source = "../snd/project.mp3")] private static const PROJECT_SND:Class;
		[Embed(source = "../snd/project_stop.mp3")] private static const PROJECT_STOP_SND:Class;
		
		// Constants
		public static const PLAYER_WIDTH:int = 10;
		public static const PLAYER_HEIGHT:int = 20;
		private static const PLAYER_COLOR:uint = 0xFFFFFF;
		private static const EPSILON:Number = 0.000001;
		private static const FOOTSTEP_WALK_RATE:Number = 0.4;
		private static const FOOTSTEP_RUN_RATE:Number = 0.3;
		private static const FOOTSTEP_INDOOR_VOLUME:Number = 0.1;
		private static const FOOTSTEP_OUTDOOR_VOLUME:Number = 0.05;
		private static const AIR_JUMP_VOLUME:Number = 0.1;
		private static const AIR_JUMP_COST:Number = 5;
		private static const GHOST_ALPHA:Number = 0.5;
		private static const PROJECTION_TIME:Number = 0.5;
		private static const PROJECTION_DURATION:Number = 15;
		private static const PROJECTION_VOLUME:Number = 0.1;
		private static const PROJECTION_STOP_VOLUME:Number = 0.1;
		private static const GHOST_BAR_WIDTH:int = 5;
		private static const GHOST_BAR_LENGTH:int = 20;
		private static const GHOST_BAR_COLOR:uint = 0xFFFFFF;
		private static const DIALOG_TYPE_TIME:Number = 0.1;
		private static const DIALOG_REST_TIME:Number = 2;
		public static const PLAYER_TYPE:String = "Player";
		public static const GHOST_TYPE:String = "Ghost";
		
		// Movement constants
		private static const WALK_SPEED:Number = 150;
		private static const RUN_SPEED:Number = 400;
		private static const WALK_RATE:Number = 1000;
		private static const JUMP_POWER:Number = 300;
		private static const JUMP_HOLD_TIME:Number = 0.2;
		private static const AIR_JUMP_POWER:Number = 400;
		
		// Physics constants
		public static const GRAVITY:Number = 1300;
		private static const DRAG:Number = 400;
		private static const MAX_COLLISION_RESOLVES:int = 200;
		private static const WALL_MIN_SLOPE:int = 1.73205081;					// 60 degree angle
		
		// Line types
		private static const TYPE_FLOOR:int = 0;
		private static const TYPE_CEILING:int = 1;
		private static const TYPE_LEFT_WALL:int = 2;
		private static const TYPE_RIGHT_WALL:int = 3;
		
		// Public variables
		public var jumpsEnabled:Boolean = true;
		public var doubleJumpEnabled:Boolean;
		public var doubleJumped:Boolean = true;
		public var onGround:Boolean = false;
		public var velocityX:int = 0;
		public var velocityY:int = 0;
		public var accelerationX:int = 0;
		public var accelerationY:int = GRAVITY;
		
		// Private variables
		private var maxWalkSpeed:Number = WALK_SPEED;
		private var jumpHoldTimer:Number = 0;
		private var activatedObjects:Vector.<IActivatable> = new Vector.<IActivatable>();
		private var footstepTimer:Number = 0;
		private var ghostFlag:Boolean;
		private var projectionTimer:Number = 0;
		private var dialogTimer:Number = 0;
		private var dialogLine:String;
		
		// Graphics
		private var playerSprite:Image;
		private var ghostBar:Image;
		private var dialogText:Text;
		
		// Objects
		private var airJumpSound:Sfx = new Sfx( AIR_JUMP_SND );
		private var projectSound:Sfx = new Sfx( PROJECT_SND );
		private var projectStopSound:Sfx = new Sfx( PROJECT_STOP_SND );
		private var footstepIndoorSounds:Array = [
			new Sfx( FOOTSTEP_INDOOR_SND ),
			new Sfx( FOOTSTEP_INDOOR_SND2 ),
			new Sfx( FOOTSTEP_INDOOR_SND ),
			new Sfx( FOOTSTEP_INDOOR_SND3 )];
		private var footstepOutdoorSounds:Array = [
			new Sfx( FOOTSTEP_OUTDOOR_SND ),
			new Sfx( FOOTSTEP_OUTDOOR_SND2 )];
		private var footstepSoundIndex:int = 0;
		
		/**
		 * Create and initialize player object
		 */
		public function Player( setX:int, setY:int, setGhost:Boolean = false ) 
		{
			ghostFlag = setGhost;
			doubleJumpEnabled = ghostFlag;
			
			// Initialize graphics
			playerSprite = Image.createRect( PLAYER_WIDTH, PLAYER_HEIGHT, PLAYER_COLOR, ghostFlag ? GHOST_ALPHA : 1 );
			//playerSprite.originX = PLAYER_WIDTH >> 1;
			//playerSprite.originY = PLAYER_HEIGHT;
			super( setX, setY, playerSprite );
			setHitbox( PLAYER_WIDTH, PLAYER_HEIGHT, playerSprite.originX, playerSprite.originY );
			
			if ( ghostFlag )
			{
				type = GHOST_TYPE;
				projectionTimer = PROJECTION_DURATION;
				ghostBar = Image.createRect( GHOST_BAR_LENGTH, GHOST_BAR_WIDTH, GHOST_BAR_COLOR, GHOST_ALPHA );
				ghostBar.x = halfWidth - (GHOST_BAR_LENGTH >> 1);
				ghostBar.y = -GHOST_BAR_WIDTH - 5;
				addGraphic( ghostBar );
			}
			else
			{
				type = PLAYER_TYPE;
				dialogText = new Text( "" );
				dialogText.x = halfWidth - (dialogText.width >> 1);
				dialogText.y = -dialogText.height - 5;
				dialogText.visible = false;
				addGraphic( dialogText );
			}
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Decrement jump hold timer
			if ( jumpHoldTimer > 0 )
			{
				if ( (Input.check( Key.UP ) || Input.check( Key.SPACE )) )
					jumpHoldTimer -= FP.elapsed;
				else
					jumpHoldTimer = 0;
			}
			
			// Get movement input
			accelerationX = 0;
			if ( Input.check( Key.LEFT ) )
			{
				accelerationX -= WALK_RATE;
			}
			if ( Input.check( Key.RIGHT ) )
			{
				accelerationX += WALK_RATE;
			}
			if ( (onGround || jumpHoldTimer > 0) && (Input.check( Key.UP ) || Input.check( Key.SPACE )) )
			{
				if ( onGround )
				{
					onGround = false;
					jumpHoldTimer = JUMP_HOLD_TIME;
					doubleJumped = false;
				}
				velocityY = -JUMP_POWER;
			}
			
			// Get double jump
			if ( doubleJumpEnabled && !onGround && !doubleJumped && jumpHoldTimer <= 0 && (Input.pressed( Key.UP ) || Input.pressed( Key.SPACE )) )
			{
				airJumpSound.play( AIR_JUMP_VOLUME );
				doubleJumped = true;
				velocityY = -AIR_JUMP_POWER;
				
				if( !ghostFlag )
					GameWorld.currentInstance.airLeft.airLeft -= 10;
			}
			
			// Handle running
			if ( Input.check( Key.SHIFT ) ) 
				maxWalkSpeed = RUN_SPEED;
			else
				maxWalkSpeed = WALK_SPEED;
			
			// Astral projection
			if ( Input.check( Key.DOWN ) && !ghostFlag )
			{
				projectionTimer += FP.elapsed;
				if ( projectionTimer > PROJECTION_TIME )
				{
					projectSound.play( PROJECTION_VOLUME );
					
					// Spawn a new ghost
					active = false;
					world.add( new Player( x, y - 1, true ) );
					projectionTimer = 0;
				}
			}
			
			// Handle ghost behavior
			if ( ghostFlag )
			{
				if ( Input.pressed( Key.DOWN ) )
				{
					KillPlayer();
				}
				
				projectionTimer -= FP.elapsed;
				ghostBar.scaleX = (projectionTimer / PROJECTION_DURATION);
				if ( projectionTimer <= 0 )
				{
					KillPlayer();
				}
			}
			
			// Handle dialog text timer
			if ( dialogTimer > 0 )
			{
				dialogTimer -= FP.elapsed;
				if ( dialogTimer <= 0 )
				{
					if ( dialogText.text.length == dialogLine.length )
					{
						dialogTimer = 0;
						dialogText.visible = false;
					}
					else
					{
						dialogText.text = dialogLine.substring( 0, dialogText.text.length + 1 );
						if ( dialogText.text.length == dialogLine.length )
							dialogTimer = DIALOG_REST_TIME;
						else
							dialogTimer = DIALOG_TYPE_TIME;
					}
				}
			}
			
			// Integrate motion
			IntegrateMotion();
			
			// Handle player moving past the edges of the screen
			if ( x - world.camera.x > FP.width )
				world.camera.x += FP.width;
			else if ( x - world.camera.x < 0 )
				world.camera.x -= FP.width;
			if ( y - world.camera.y > FP.height )
				world.camera.y += FP.height;
			else if ( y - world.camera.y < 0 )
				world.camera.y -= FP.height;
			
			// Handle footstep sounds
			footstepTimer += FP.elapsed;
			if ( onGround && Math.abs( velocityX ) >= WALK_SPEED )
			{
				if( footstepTimer > (maxWalkSpeed == WALK_SPEED ? FOOTSTEP_WALK_RATE : FOOTSTEP_RUN_RATE) )
				{
					var footstepSounds:Array = (GameWorld.currentInstance.outdoor ? footstepOutdoorSounds : footstepIndoorSounds);
					footstepSoundIndex = (footstepSoundIndex + 1) % footstepSounds.length;
					footstepSounds[footstepSoundIndex].play( GameWorld.currentInstance.outdoor ? FOOTSTEP_OUTDOOR_VOLUME : FOOTSTEP_INDOOR_VOLUME );
					
					footstepTimer = 0;
				}
			}
			
			// Check for interaction with game objects
			var objects:Vector.<IActivatable> = new Vector.<IActivatable>();
			GetActivatedObjects( x, y, objects );
			if ( ghostFlag )
				GetActivatedObjects( GameWorld.currentInstance.player.x, GameWorld.currentInstance.player.y, objects );
			
			for each( var object:IActivatable in objects )
			{
				if ( !object.activated )
				{
					// Activate objects that aren't activated yet
					object.activated = true;
					activatedObjects.push( object );
				}
			}
			
			// Remove objects that are no longer activated
			for ( var i:int = 0; i < activatedObjects.length; ++i )
			{
				if ( objects.indexOf( activatedObjects[i] ) == -1 )
				{
					if( (activatedObjects[i] as Entity).world != null )
						activatedObjects[i].activated = false;
					activatedObjects.splice( i--, 1 );
				}
			}
			
			super.update();
		}
		
		/**
		 * Deactivate all activated objects when a ghost is removed from the
		 * world
		 */
		override public function removed():void 
		{
			if ( ghostFlag )
			{
				// Remove objects that are no longer activated
				for ( var i:int = 0; i < activatedObjects.length; ++i )
				{
					activatedObjects[i].activated = false;
				}
			}
			
			super.removed();
		}
		
		/**
		 * Return all activated objects for a specified player position
		 * 
		 * @param	x
		 * Player position to check
		 * @param	y
		 * Player position to check
		 * @param	objects
		 * Vector of objects to fill with activated objects
		 */
		private function GetActivatedObjects( x:Number, y:Number, objects:Vector.<IActivatable> ):void
		{
			collideInto( GameWorld.ACTIVATABLE_TYPE, x, y, objects );
			
			// Check for circle collision on objects
			for ( var i:int = 0; i < objects.length; ++i )
			{
				if ( objects[i] is ICollisionCircle )
				{
					// If the player is outside of the collision circle, remove
					// the object from the activated list
					if ( distanceToPoint( (objects[i] as Entity).x, (objects[i] as Entity).y, true ) > (objects[i] as ICollisionCircle).radius )
					{
						objects.splice( i, 1 );
						i--;
					}
				}
			}
		}
		
		/**
		 * Handle player death
		 */
		private function KillPlayer():void
		{
			if ( ghostFlag )
			{
				world.remove( this );
				GameWorld.currentInstance.player.active = true;
				projectStopSound.play(PROJECTION_STOP_VOLUME);
			}
			else
			{
				GameWorld.TriggerEvent( "GameOver" );
			}
		}
		
		/**
		 * Show dialog text
		 */
		public function ShowDialog( showText:String ):void
		{
			dialogLine = showText;
			dialogText.text = dialogLine;
			dialogText.x = halfWidth - (dialogText.textWidth >> 1);
			dialogText.y = -dialogText.height - 5;
			dialogText.text = "";
			dialogText.visible = true;
			
			dialogTimer = DIALOG_TYPE_TIME;
		}
		
		/**
		 * Integrate velocity and acceleration into player motion, and handle
		 * collision detection
		 */
		private function IntegrateMotion():void
		{
			// Apply drag and movement speed cap
			if ( velocityX != 0 )
			{
				// Cap horizontal velocity at max walking speed
				if ( velocityX > maxWalkSpeed )
					velocityX = maxWalkSpeed;
				else if ( velocityX < -maxWalkSpeed )
					velocityX = -maxWalkSpeed;
				
				// Apply drag, capped at current player velocity in a direction
				// opposite to the player's horizontal movement
				var drag:Number = Math.min( Math.abs( velocityX ) / FP.elapsed, DRAG ) * (velocityX > 0 ? -1 : 1);
				accelerationX += drag;
			}
			
			// Integrate acceleration and move object by velocity vector
			var integrateX:Number = (accelerationX >> 1) * FP.elapsed;
			var integrateY:Number = (accelerationY >> 1) * FP.elapsed;
			
			velocityX += integrateX;
			velocityY += integrateY;
			
			// Move with collision detection
			CollisionDetect( x + velocityX * FP.elapsed, y + velocityY * FP.elapsed );
			
			velocityX += integrateX;
			velocityY += integrateY;
		}
		
		/**
		 * Move to a destination point and check for collisions on the way
		 * 
		 * @param	toX
		 * Destination point
		 * @param	toY
		 * Destination point
		 */
		private function CollisionDetect( toX:Number, toY:Number ):void
		{
			// Check that the player moved
			if ( x == toX && y == toY )
				return;
			
			// Create collision points for the four corners of the hitbox
			var from:Vector.<Point> = new Vector.<Point>(4);
			from[0] = new Point( x, y );
			from[1] = new Point( x + PLAYER_WIDTH, y );
			from[2] = new Point( x, y + PLAYER_HEIGHT );
			from[3] = new Point( x + PLAYER_WIDTH, y + PLAYER_HEIGHT );
			
			// Create destination points for the four corners of the hitbox
			var dest:Vector.<Point> = new Vector.<Point>(4);
			dest[0] = new Point( toX, toY );
			dest[1] = new Point( toX + PLAYER_WIDTH, toY );
			dest[2] = new Point( toX, toY + PLAYER_HEIGHT );
			dest[3] = new Point( toX + PLAYER_WIDTH, toY + PLAYER_HEIGHT );
			
			var points:Vector.<int> = new Vector.<int>();
			var leavingGround:Boolean = onGround;
			onGround = false;
			var collided:Boolean = true;
			var resolveCount:int = 0;
			
			// Get all lines in the world
			var lines:Vector.<Line> = GameWorld.currentInstance.collisionLines;
			
			// Continue checking until there are no collisions to resolve
			while ( collided )
			{
				if ( resolveCount > MAX_COLLISION_RESOLVES )
					break;
				
				resolveCount++;
				collided = false;
				
				// Check each line for collision
				for each( var line:Line in lines )
				{
					// Check that the line exists
					if ( line.end1.x == line.end2.x && line.end1.y == line.end2.y )
						continue;
					
					// Boundary check the motion line against the collision line
					if ( Math.min( from[0].x, dest[0].x ) <= Math.max( line.end1.x, line.end2.x )
						&& Math.max( from[3].x, dest[3].x ) >= Math.min( line.end1.x, line.end2.x )
						&& Math.min( from[0].y, dest[0].y ) <= Math.max( line.end1.y, line.end2.y )
						&& Math.max( from[3].y, dest[3].y ) >= Math.min( line.end1.y, line.end2.y ) )
					{
						// Determine line type
						var lineType:int;
						points.length = 0;
						if ( line.end1.x == line.end2.x || Math.abs( (line.end2.y - line.end1.y) / (line.end2.x - line.end1.x) ) > WALL_MIN_SLOPE )
						{
							if ( line.end2.y > line.end1.y )
							{
								lineType = TYPE_LEFT_WALL;
								points.push( 0, 2 );
							}
							else
							{
								lineType = TYPE_RIGHT_WALL;
								points.push( 1, 3 );
							}
						}
						else
						{
							if ( line.end2.x > line.end1.x )
							{
								lineType = TYPE_FLOOR;
								points.push( 2, 3 );
							}
							else
							{
								lineType = TYPE_CEILING;
								points.push( 0, 1 );
							}
						}
						
						for ( var i:int = 0; i < points.length; ++i )
						{
							collided = ResolveCollision( from[points[i]], dest[points[i]], line, lineType );
							if ( collided )
							{
								var diffX:Number = dest[points[i]].x - from[points[i]].x;
								var diffY:Number = dest[points[i]].y - from[points[i]].y;
								
								for ( var j:int = 0; j < 4; ++j )
								{
									dest[j].x = from[j].x + diffX;
									dest[j].y = from[j].y + diffY;
								}
								
								break;
							}
						}
						
						if ( collided )
							break;
					}
				}
			}
			
			// Move to new position
			x = dest[0].x;
			y = dest[0].y;
		}
		
		/**
		 * Resolve a collision with a line
		 * 
		 * @param	dest
		 * @param	line
		 */
		private function ResolveCollision( from:Point, dest:Point, line:Line, lineType:int ):Boolean
		{
			// Get the following vectors:
			// - a: vector from line point 1 to line point 2
			// - b: bector from line point 1 to motion start point
			// - c: vector from line point 1 to motion end point
			var a:Point = new Point( line.end2.x - line.end1.x, line.end2.y - line.end1.y );
			var b:Point = new Point( from.x - line.end1.x, from.y - line.end1.y );
			var c:Point = new Point( dest.x - line.end1.x, dest.y - line.end1.y );
			
			// Get scalar projection of b and c onto a's orthogonal
			// vector, use the signs to determine which side of a
			// the motion start and end points fall on
			var o1:Number = (a.x * b.y - a.y * b.x);
			var o2:Number = (a.x * c.y - a.y * c.x);
			
			// Line collision is one way, only capture collisions
			// where motion start is left of the line and motion end
			// is right
			if ( o1 < 0 && o2 > 0 )
			{
				// Get the intersection point on a
				var v:Number = ((dest.x - from.x) * b.y - (dest.y - from.y) * b.x) / (a.y * (dest.x - from.x) - a.x * (dest.y - from.y));
				
				// If the intersection point is inside the line
				// segment, the lines intersect
				if ( v >= 0 && v <= 1 )
				{
					// If collided with a danger line, kill player
					if ( line.danger )
						KillPlayer();
					
					// Workaround - if X velocity is zero, then
					// "stick" to the surface and don't project a
					// new position - this allows sliding along the
					// ground when moving horizontally, but stops
					// sliding along slopes due to gravity
					if ( lineType == TYPE_LEFT_WALL || lineType == TYPE_RIGHT_WALL || Math.abs( velocityX ) > EPSILON )
					{
						// Project target point onto line
						v = (a.x * c.x + a.y * c.y) / (a.x * a.x + a.y * a.y)
					}
					
					// Set new target point on the line at scalar
					// position v
					dest.x = line.end1.x + a.x * v;
					dest.y = line.end1.y + a.y * v;
					
					if ( lineType == TYPE_FLOOR )
					{
						velocityY = 0;
						onGround = true;
						
						// Move point up by EPSILON to avoid the point
						// being congruent with the line
						dest.y -= EPSILON;
					}
					else if ( lineType == TYPE_CEILING )
					{
						velocityY = Math.max( velocityY, 0 );
						jumpHoldTimer = 0;
						dest.y += EPSILON;
					}
					else if ( lineType == TYPE_LEFT_WALL )
					{
						velocityX = 0;
						dest.x += EPSILON;
					}
					else if( lineType == TYPE_RIGHT_WALL )
					{
						velocityX = 0;
						dest.x -= EPSILON;
					}
					
					// Set collision flag
					return true;
				}
			}
			
			return false;
		}
		
	}

}