package  
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.utils.*;
	
	/**
	 * Collision line entity
	 * 
	 * @author Switchbreak
	 */
	public class Line 
	{
		// Public constants
		public static const COLLISION_TYPE:String = "Line";
		
		// Public variables
		public var end1:Point;
		public var end2:Point;
		public var parent:Entity = null;
		public var danger:Boolean = false;
		
		/**
		 * Create and initialize line entity
		 */
		public function Line( setEnd1:Point, setEnd2:Point, setDanger:Boolean = false ) 
		{
			// Set object values
			danger = setDanger;
			end1 = setEnd1;
			end2 = setEnd2;
		}
		
		/**
		 * Finds the distance between a point and a line, returns true if it is
		 * under the threshold
		 * 
		 * @param	x
		 * Position of the point
		 * @param	y
		 * Position of the point
		 * @param	threshold
		 * Squared distance threshold to check
		 */
		public function DistanceToLine( p:Point, threshold:Number ):Boolean
		{
			// Get the following vectors:
			// a - vector from line point 1 to line point 2
			// b - vector from line point 1 to (x, y)
			var a:Point = new Point( end2.x - end1.x, end2.y - end1.y );
			var b:Point = new Point( p.x - end1.x, p.y - end1.y );
			
			// Scalar project b onto a to get closest point to (x, y) on line
			var u:Number = (a.x * b.x + a.y * b.y) / (a.x * a.x + a.y * a.y);
			
			// Clamp closest point to line segment and get position
			if ( u < 0 )
				u = 0;
			else if ( u > 1 )
				u = 1;
			var c:Point = new Point( end1.x + a.x * u, end1.y + a.y * u );
			
			// Check distance against squared theshold
			if ( (p.x - c.x) * (p.x - c.x) + (p.y - c.y) * (p.y - c.y) <= threshold )
			{
				p.x = c.x;
				p.y = c.y;
				return true;
			}
			else
				return false;
		}
	}

}