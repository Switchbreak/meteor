package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.tweens.misc.VarTween;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.FP;
	
	/**
	 * Object to fade out on game over
	 * @author Switchbreak
	 */
	public class FadeOut extends Entity 
	{
		// Constants
		private static const FADE_DURATION:Number = 1.0;
		
		// Graphics
		private var fadeCurtain:Image;
		
		/**
		 * Create and initialize object
		 */
		public function FadeOut() 
		{
			// Show on top of everything
			layer = -30000;
			
			// Create graphics
			fadeCurtain = Image.createRect( FP.width, FP.height, 0, 0 );
			fadeCurtain.scrollX = 0;
			fadeCurtain.scrollY = 0;
			
			super( 0, 0, fadeCurtain );
			
			var fadeTween:VarTween = new VarTween( FadeComplete, ONESHOT );
			fadeTween.tween( fadeCurtain, "alpha", 1.0, FADE_DURATION, Ease.cubeOut );
			addTween( fadeTween, true );
		}
		
		/**
		 * Fade out is complete, show the text boxes
		 */
		private function FadeComplete():void
		{
			world.remove( this );
			GameWorld.TriggerEvent( "Restart" );
		}
		
	}

}