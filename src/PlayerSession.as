package  
{
	import Interfaces.ISavable;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Data;
	
	/**
	 * Player session for save games
	 * 
	 * @author Switchbreak
	 */
	public class PlayerSession 
	{
		// Player position
		public var areaIndex:int = -1;
		public var playerX:int;
		public var playerY:int;
		
		// Game state
		public var inventory:Vector.<String>;
		public var outdoor:Boolean;
		public var airLeft:Number;
		
		// Game object states
		public var serializedStates:XML;
		
		// Private variables
		private var gameWorld:GameWorld;
		
		/**
		 * Save the current gamestate to a session object
		 * 
		 * @param	gameWorld
		 * Game world to save the session of
		 * @param	save
		 * True to save the current state, false to load a saved state
		 * @param	gameObjects
		 * Serialized list of game objects to save
		 */
		public function PlayerSession( setGameWorld:GameWorld, save:Boolean = true, gameObjects:XML = null )
		{
			gameWorld = setGameWorld;
			
			if ( save )
			{
				// Get player position
				playerX = gameWorld.player.x;
				playerY = gameWorld.player.y - 1;
				
				// Get game state
				outdoor = gameWorld.outdoor;
				airLeft = gameWorld.airLeft.airLeft;
				
				// TODO: Serialize current player inventory
				
				// Serialize all stateful objects in the game
				if ( gameObjects == null )
				{
					var objects:Vector.<ISavable> = new Vector.<ISavable>();
					gameWorld.getClass( ISavable, objects );
					
					serializedStates = new XML( <objects/> );
					for each( var object:ISavable in objects )
					{
						serializedStates.appendChild( object.GetSaveObject() );
					}
				}
				else
				{
					serializedStates = gameObjects;
				}
				
				SaveSession();
			}
			else
			{
				LoadSession();
			}
		}
		
		/**
		 * Restore a saved game state session
		 */
		public function RestoreSession():void
		{
			// Restore player position
			gameWorld.player.x = playerX;
			gameWorld.player.y = playerY;
			
			// Restore game state
			gameWorld.airLeft.airLeft = airLeft;
			gameWorld.outdoor = outdoor;
			
			// Activate game world
			gameWorld.fadingOut = false;
			gameWorld.player.active = true;
			
			// Set camera
			gameWorld.camera.x = Math.floor( playerX / FP.width ) * FP.width;
			gameWorld.camera.y = Math.floor( playerY / FP.height ) * FP.height;
			
			// Remove all stateful objects in game
			var objects:Vector.<ISavable> = new Vector.<ISavable>();
			gameWorld.getClass( ISavable, objects );
			gameWorld.removeList( objects );
			
			// Restore all serialized stateful objects from session
			for each( var node:XML in serializedStates.children() )
			{
				gameWorld.DeserializeObject( node );
			}
		}
		
		/**
		 * Save a session to the client machine
		 */
		public function SaveSession():void
		{
			Data.writeInt( "area", 1 );
			Data.writeInt( "playerX", playerX );
			Data.writeInt( "playerY", playerY );
			Data.writeBool( "outdoor", outdoor );
			Data.writeString( "airLeft", airLeft.toString() );
			Data.writeString( "objects", serializedStates.toXMLString() );
			
			Data.save( "lastSession" );
		}
		
		/**
		 * Load a saved session from the client machine
		 */
		public function LoadSession():void
		{
			if ( !CheckSession() )
				return;
			
			Data.load( "lastSession" );
			
			playerX = Data.readInt( "playerX" );
			playerY = Data.readInt( "playerY" );
			outdoor = Data.readBool( "outdoor" );
			airLeft = Number( Data.readString( "airLeft" ) );
			serializedStates = new XML( Data.readString( "objects" ) );
			
			RestoreSession();
		}
		
		/**
		 * Check if a saved session exists
		 */
		public static function CheckSession():Boolean
		{
			Data.load( "lastSession" );
			
			if ( Data.readInt( "area" ) > 0 )
				return true;
			else
				return false;
		}
	}

}