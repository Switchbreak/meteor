package  
{
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.geom.Point;
	import flash.utils.ByteArray;
	import flash.utils.Dictionary;
	import GameObjects.Logic.AndGate;
	import GameObjects.Logic.OnceGate;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.World;
	import net.flashpunk.FP;
	import net.flashpunk.utils.*;
	import mx.resources.ResourceManager;
	import flash.net.*;
	import Interfaces.*;
	import flash.ui.Keyboard;
	import Utils.*;
	import GameObjects.*;
	import EditorUI.*;
	
	/**
	 * Game world object
	 * 
	 * @author Switchbreak
	 */
	[ResourceBundle("resources")]
	public class GameWorld extends World 
	{
		// Embeds
		[Embed(source = "../img/Astronomy.png")] private static const BG_ASTRONOMY:Class;
		[Embed(source = "../img/Stars.png")] private static const BG_STARS:Class;
		[Embed(source = "../xml/level.xml", mimeType = "application/octet-stream")] private static const LEVEL_1:Class;
		
		// Public Constants
		public static const HUD_LAYER:int = -1000;
		public static const BACKGROUND_LAYER:int = 1;
		public static const ACTIVATABLE_TYPE:String = "Activatable";
		
		// Constants
		private static const LINE_COLOR:uint = 0x00ff00;
		private static const LINE_DANGER_COLOR:uint = 0xff0000;
		private static const BACKGROUND_COLOR:uint = 0x202020;
		
		// Game states
		private static const STATE_NONE:int = 0;
		private static const STATE_DRAWING:int = 1;
		private static const STATE_ERASING:int = 2;
		private static const STATE_PLACING:int = 3;
		private static const STATE_DANGER:int = 4;
		private static const STATE_LAYER_LIST:int = 5;
		private static const STATE_PROPERTIES:int = 6;
		
		// Statics
		public static var currentInstance:GameWorld;
		
		// Public variables
		public var inventory:Inventory = new Inventory();
		public var player:Player;
		public var airLeft:AirTimer;
		public var fadingOut:Boolean = false;
		public var realOutdoor:Boolean = false;
		public var captureKeys:Boolean = true;
		public var captureClicks:Boolean = true;
		public var collisionLines:Vector.<Line> = new Vector.<Line>();
		public var layerSources:Vector.<ByteArray> = new Vector.<ByteArray>();
		
		// Private variables
		private var _outdoor:Boolean = false;
		private var _state:int = STATE_NONE;
		private var _showHandles:Boolean = false;
		private var currentLevel:int = 0;
		private var uploadFile:FileReference;
		private var lastSession:PlayerSession;
		
		// Game objects
		private var drawLines:DrawLines;
		private var eraser:Eraser;
		private var placeObject:PlaceObject;
		private var layerList:ImageLayerList;
		private var propertyPane:PropertyPane;
		private var gameState:Text;
		public var imageLayers:Vector.<Entity> = new Vector.<Entity>();
		
		/**
		 * Create and initialize game world object
		 */
		public function GameWorld( loadCheckpoint:Boolean = false ) 
		{
			//FP.console.enable();
			FP.screen.color = BACKGROUND_COLOR;
			
			// Create game state display object
			gameState = new Text( ResourceManager.getInstance().getString( "resources", "GAME_STATE_NONE" ) );
			gameState.scrollX = 0;
			gameState.scrollY = 0;
			addGraphic( gameState, HUD_LAYER );
			
			// Create player object
			player = add( new Player( FP.halfWidth, FP.height ) ) as Player;
			
			// Create air timer object
			airLeft = add( new AirTimer() ) as AirTimer;
			
			// Load first level
			if ( !loadCheckpoint )
			{
				LoadLevelXML( new XML( new LEVEL_1 ), true );
			}
			else
			{
				LoadLevelXML( new XML( new LEVEL_1 ), false );
				lastSession = new PlayerSession( this, false );
			}
		}
		
		/**
		 * Run when game world is activated
		 */
		override public function begin():void 
		{
			currentInstance = this;
			
			// Add listener on save and load keys
			FP.stage.addEventListener(KeyboardEvent.KEY_DOWN, SaveLoadEvent);
		}
		
		/**
		 * Handle game input
		 */
		override public function update():void 
		{
			if ( captureKeys )
			{
				// Set state if the hotkey is pressed
				if ( Input.released( Key.D ) )
				{
					// Drawing hotkey was pressed, toggle drawing state
					if ( state == STATE_DRAWING )
						state = STATE_NONE;
					else
						state = STATE_DRAWING;
				}
				if ( Input.released( Key.E ) )
				{
					// Erase hotkey was pressed, toggle eraser state
					if ( state == STATE_ERASING )
						state = STATE_NONE;
					else
						state = STATE_ERASING;
				}
				if ( Input.released( Key.K ) )
				{
					// Danger line drawing hotkey was pressed, toggle drawing state
					if ( state == STATE_DANGER )
						state = STATE_NONE;
					else
						state = STATE_DANGER;
				}
				if ( Input.released( Key.L ) )
				{
					// Layer list hotkey was pressed, toggle layer list state
					if ( state == STATE_LAYER_LIST )
						state = STATE_NONE;
					else
						state = STATE_LAYER_LIST;
				}
				
				// Set player position
				if ( Input.released( Key.P ) )
				{
					player.x = mouseX;
					player.y = mouseY;
					player.active = true;
					
					lastSession.playerX = player.x;
					lastSession.playerY = player.y;
				}
				
				// Clear level
				if ( Input.released( Key.C ) )
				{
					ClearLevel();
				}
				
				// Toggle environment
				if ( Input.released( Key.O ) )
				{
					outdoor = !outdoor;
				}
				if ( Input.released( Key.TAB ) )
				{
					showHandles = !showHandles;
				}
				if ( Input.released( Key.R ) )
				{
					lastSession.LoadSession();
				}
				if ( Input.released( Key.H ) )
				{
					lastSession = new PlayerSession( this, true );
				}
				
				// Place game objects
				if ( !Input.check( Key.CONTROL ) && !Input.check( Key.SHIFT ) )
				{
					if ( Input.released( Key.DIGIT_1 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new KeyObject() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_2 ) )
					{
						state = STATE_NONE;
						add( new Door() );
					}
					if ( Input.released( Key.DIGIT_3 ) )
					{
						state = STATE_NONE;
						add( new Elevator() );
					}
					if ( Input.released( Key.DIGIT_4 ) )
					{
						state = STATE_NONE;
						add( new Button() );
					}
					if ( Input.released( Key.DIGIT_5 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new OxygenGenerator() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_6 ) )
					{
						state = STATE_NONE;
						add( new Lifter() );
					}
					if ( Input.released( Key.DIGIT_7 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new Timer() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_8 ) )
					{
						state = STATE_NONE;
						add( new PressurePlate() );
					}
					if ( Input.released( Key.DIGIT_9 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new Spawn() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_0 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new AndGate() ), EndState ) ) as PlaceObject;
					}
				}
				else if ( Input.check( Key.CONTROL ) )
				{
					if ( Input.released( Key.DIGIT_1 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new ImageSwitcher( "LeftBranch" ) ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_2 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new ImageSwitcher( "TreeLeft" ) ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_3 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new ImageSwitcher( "TreeRight" ) ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_4 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new ImageSwitcher( "RightBranch" ) ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_5 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new ImageSwitcher( "Plant" ) ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_6 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new Checkpoint() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_7 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new OnceGate() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_8 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new SoundEmitter() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_9 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new Earthquake() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_0 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new Lightning() ), EndState ) ) as PlaceObject;
					}
				}
				else if ( Input.check( Key.SHIFT ) )
				{
					if ( Input.released( Key.DIGIT_1 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new ColorCycle() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_2 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new ColorShow() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_3 ) )
					{
						state = STATE_PLACING;
						placeObject = add( new PlaceObject( add( new Cat() ), EndState ) ) as PlaceObject;
					}
					if ( Input.released( Key.DIGIT_4 ) )
					{
						state = STATE_NONE;
						add( new Path() );
					}
				}
				
				// Handle camera movement
				if ( Input.check( Key.CONTROL ) )
				{
					if ( Input.released( Key.LEFT ) )
					{
						player.active = false;
						camera.x -= FP.width;
					}
					if ( Input.released( Key.RIGHT ) )
					{
						player.active = false;
						camera.x += FP.width;
					}
					if ( Input.released( Key.UP ) )
					{
						player.active = false;
						camera.y -= FP.height;
					}
					if ( Input.released( Key.DOWN ) )
					{
						player.active = false;
						camera.y += FP.height;
					}
				}
			}
			
			// Select game objects if edit handles are visible
			if ( Input.mousePressed && captureClicks )
			{
				if ( state == STATE_PROPERTIES )
				{
					if( Input.mouseX < propertyPane.x )
						state = STATE_NONE;
				}
				else if ( state == STATE_NONE && showHandles )
				{
					var objects:Vector.<Entity> = new Vector.<Entity>();
					getClass( ISavable, objects );
					for each( var object:Entity in objects )
					{
						if ( object.collidePoint( object.x, object.y, mouseX, mouseY ) )
						{
							propertyPane = add( new PropertyPane( object ) ) as PropertyPane;
							state = STATE_PROPERTIES;
							break;
						}
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Draw collision lines in edit mode
		 */
		override public function render():void 
		{
			super.render();
			
			if ( _showHandles )
			{
				// Draw all the collision lines
				for each( var collisionLine:Line in collisionLines )
				{
					if( collisionLine.parent == null )
						Draw.line( collisionLine.end1.x, collisionLine.end1.y, collisionLine.end2.x, collisionLine.end2.y, collisionLine.danger ? LINE_DANGER_COLOR : LINE_COLOR );
				}
			}
		}
		
		/**
		 * Trigger a game event
		 * 
		 * @param	target
		 * Name of game event to trigger
		 */
		public static function TriggerEvent( target:String, event:String = null, value:int = 0 ):void
		{
			// Pick action by trigger target
			switch( target )
			{
				case "TelescopeLook":
					// Show the telescope view cutscene
					FP.world.add( new TelescopeView() );
					break;
				case "AirEscape":
					// Show the remaining air timer
					currentInstance.realOutdoor = true;
					currentInstance.outdoor = true;
					break;
				case "AirClose":
					// Show the remaining air timer
					currentInstance.realOutdoor = false;
					currentInstance.outdoor = false;
					break;
				case "GameOver":
					// Fade out and show the game over screen
					if ( !currentInstance.fadingOut )
					{
						currentInstance.fadingOut = true;
						currentInstance.player.active = false;
						FP.world.add( new FadeOut() );
					}
					
					break;
				case "Restart":
					// Start over from first level
					currentInstance.lastSession.RestoreSession();
					
					break;
				case "Checkpoint":
					// Save current state to session
					currentInstance.lastSession = new PlayerSession( currentInstance );
					
					break;
				case "Speak":
					currentInstance.player.ShowDialog( event );
					break;
				default:
					var object:ITriggerable = FP.world.getInstance( target ) as ITriggerable;
					if ( object != null )
					{
						object.Trigger( event, value );
					}
			}
		}
		
		/**
		 * Clears all level objects to start editing a new level
		 */
		private function ClearLevel():void
		{
			// Remove all background images
			removeList( imageLayers );
			imageLayers.length = 0;
			layerSources.length = 0;
			
			// Remove all current collision lines in the scene
			collisionLines.length = 0;
			
			// Remove all game objects
			var objects:Vector.<ISavable> = new Vector.<ISavable>();
			getClass( ISavable, objects );
			removeList( objects );
			
			outdoor = false;
			airLeft.airLeft = AirTimer.MAX_AIR;
			camera.x = 0;
			camera.y = 0;
			player.active = false;
		}
		
		/**
		 * Saves the current level in memory to an XML file on the client's
		 * hard drive
		 */
		private function SaveLevelXML():void
		{
			// Create level XML object
			var levelXML:XML = <scene />;
			
			// Write the player's starting position to the XML
			levelXML[0].appendChild( <player x={ lastSession.playerX } y={ lastSession.playerY } /> );
			
			// Append all background images to the XML document
			levelXML[0].appendChild( <images /> );
			for ( var i:int = 0; i < imageLayers.length; ++i )
			{
				if ( layerSources[i] != null )
				{
					levelXML[0].images[0].appendChild( <image type="base64" src={Base64.encodeByteArray( layerSources[i] )} x={imageLayers[i].x} y={imageLayers[i].y} /> );
				}
				else
				{
					levelXML[0].images[0].appendChild( <image type="embed" src={imageLayers[i].name} x={imageLayers[i].x} y={imageLayers[i].y} /> );
				}
			}
			
			// Get all game objects
			var objects:Vector.<ISavable> = new Vector.<ISavable>();
			getClass( ISavable, objects );
			
			// Append the objects to the XML document
			levelXML[0].appendChild( <objects /> );
			for each( var object:ISavable in objects )
			{
				// Save object by class
				levelXML[0].objects[0].appendChild( object.GetSaveObject() );
			}
			
			// Append the lines to the XML document
			levelXML[0].appendChild( <lines /> );
			for each( var line:Line in collisionLines )
			{
				if ( line.parent == null )
				{
					var lineXML:XML = <line x1={line.end1.x} y1={line.end1.y} x2={line.end2.x} y2={line.end2.y} />
					if ( line.danger )
						lineXML.@danger = "true";
					
					levelXML[0].lines[0].appendChild( lineXML );
				}
			}
			
			// Save XML file to client's computer
			new FileReference().save( levelXML, "level.xml" );
		}
		
		/**
		 * An XML file has been uploaded, load the level from it
		 * 
		 * @param	levelXML
		 * XML data containing level data
		 * @param	playerReset
		 * Flag specifying whether or not the player position should be reset
		 */
		private function LoadLevelXML( levelXML:XML, playerReset:Boolean = true ):void
		{
			// Reset state and clear all level objects
			state = STATE_NONE;
			ClearLevel();
			
			// Load all background images
			for each( var node:XML in levelXML.images.image )
			{
				// Select background image
				var src:String = node.@src;
				var type:String = node.@type;
				
				if ( type == "base64" )
				{
					// Import image data from Base64 string
					var imageData:ByteArray = Base64.decodeToByteArray( src );
					layerSources.push( imageData );
					
					var imageLayer:Entity = add( new Entity( node.@x, node.@y ) );
					imageLayer.layer = BACKGROUND_LAYER;
					imageLayers.push( imageLayer );
					
					new ImageLoader( imageLayer, imageData );
				}
				else
				{
					layerSources.push( null );
					var imgSource:Class = null;
					
					// Load embedded image by name
					switch( src )
					{
						case "astronomy":
							imgSource = BG_ASTRONOMY;
							break;
						case "stars":
							imgSource = BG_STARS;
							break;
					}
					
					// Create image layer
					imageLayer = addGraphic( new Stamp( imgSource ), BACKGROUND_LAYER, node.@x, node.@y );
					imageLayer.setHitboxTo( imageLayer.graphic );
					imageLayer.name = src;
					imageLayers.push( imageLayer );
				}
			}
			
			// Load all collision lines
			for each( node in levelXML.lines.line )
			{
				collisionLines.push( new Line( new Point( node.@x1, node.@y1 ), new Point( node.@x2, node.@y2 ), (node.@danger == "true") ) );
			}
			
			// Set player starting position
			if ( playerReset )
			{
				// Load all game objects
				for each( node in levelXML.objects.children() )
				{
					DeserializeObject( node );
				}
				
				player.x = levelXML.player.@x;
				player.y = levelXML.player.@y;
				player.velocityX = 0;
				player.velocityY = 0;
				
				player.active = true;
				outdoor = false;
				
				// Save the starting position as a player session
				lastSession = new PlayerSession( this, true, levelXML.objects[0] );
			}
		}
		
		/**
		 * Deserialize a game object from XML and instantiate it as an entity
		 * 
		 * @param	node
		 * XML node to deserialize
		 * @return
		 * Entity created
		 */
		public function DeserializeObject( node:XML ):Entity
		{
			var object:Entity;
			
			switch( node.localName() )
			{
				case "key":
					object = add( KeyObject.Deserialize( node ) );
					break;
				case "door":
					object = add( Door.Deserialize( node ) );
					break;
				case "elevator":
					object = add( Elevator.Deserialize( node ) );
					break;
				case "button":
					object = add( Button.Deserialize( node ) );
					break;
				case "tank":
					object = add( OxygenGenerator.Deserialize( node ) );
					break;
				case "lifter":
					object = add( Lifter.Deserialize( node ) );
					break;
				case "timer":
					object = add( Timer.Deserialize( node ) );
					break;
				case "pressureplate":
					object = add( PressurePlate.Deserialize( node ) );
					break;
				case "spawn":
					object = add( Spawn.Deserialize( node ) );
					break;
				case "andgate":
					object = add( AndGate.Deserialize( node ) );
					break;
				case "imageswitcher":
					object = add( ImageSwitcher.Deserialize( node ) );
					break;
				case "checkpoint":
					object = add( Checkpoint.Deserialize( node ) );
					break;
				case "oncegate":
					object = add( OnceGate.Deserialize( node ) );
					break;
				case "soundemitter":
					object = add( SoundEmitter.Deserialize( node ) );
					break;
				case "earthquake":
					object = add( Earthquake.Deserialize( node ) );
					break;
				case "lightning":
					object = add( Lightning.Deserialize( node ) );
					break;
				case "colorcycler":
					object = add( ColorCycle.Deserialize( node ) );
					break;
				case "colorshow":
					object = add( ColorShow.Deserialize( node ) );
					break;
				case "cat":
					object = add( Cat.Deserialize( node ) );
					break;
				case "path":
					object = add( Path.Deserialize( node ) );
					break;
			}
			
			return object;
		}
		
		/**
		 * End the current state
		 */
		private function EndState():void
		{
			state = STATE_NONE;
		}
		
		/**
		 * Clean up when world is deactivated
		 */
		override public function end():void 
		{
			// Remove keyboard event listener
			FP.stage.removeEventListener(KeyboardEvent.KEY_DOWN, SaveLoadEvent);
			
			super.end();
		}
		
		//
		// Event handlers
		//
		
		/**
		 * Function to handle F11 and F12 keypresses and show the save or load
		 * dialog box.
		 * 
		 * This is to work around Flash's security restrictions on using
		 * FileReference - on the web, it requires that the FileReference be
		 * created in response to a user event.
		 */
		public function SaveLoadEvent( keyEvent:KeyboardEvent ):void
		{
			if ( !captureKeys )
				return;
			
			// Save or load level XML
			if ( keyEvent.keyCode == Keyboard.F11 )
			{
				SaveLevelXML();
			}
			if ( keyEvent.keyCode == Keyboard.F12 )
			{
				if ( uploadFile == null )
				{
					// Set file filter and show browse dialog
					uploadFile = new FileReference();
					var xmlFilter:FileFilter = new FileFilter( "XML Files (*.xml)", "*.xml" );
					if ( uploadFile.browse( [xmlFilter] ) )
					{
						// Set event handler for file selected
						uploadFile.addEventListener(Event.SELECT, OnFileSelected);
						uploadFile.addEventListener(Event.CANCEL, OnFileCancel);
						uploadFile.addEventListener(Event.COMPLETE, OnLevelLoad );
					}
				}
			}
			
			// Place background image
			if ( keyEvent.keyCode == Keyboard.I )
			{
				if ( uploadFile == null )
				{
					// Set file filter and show browse dialog
					uploadFile = new FileReference();
					xmlFilter = new FileFilter( "PNG Images (*.png)", "*.png" );
					if ( uploadFile.browse( [xmlFilter] ) )
					{
						// Set event handler for file selected
						uploadFile.addEventListener(Event.SELECT, OnFileSelected);
						uploadFile.addEventListener(Event.CANCEL, OnFileCancel);
						uploadFile.addEventListener(Event.COMPLETE, OnImageLoad );
					}
				}
			}
		}
		
		/**
		 * A file to upload has been selected, upload it
		 * 
		 * @param	evt
		 * Event handle
		 */
		private function OnFileSelected( evt:Event ):void
		{
			// Start uploading
			uploadFile.load();
		}
		
		/**
		 * A file upload dialog box has been canceled, remove it
		 * 
		 * @param	evt
		 * Event handle
		 */
		private function OnFileCancel( evt:Event ):void
		{
			uploadFile = null;
		}
		
		/**
		 * An XML level file has been uploaded, import it
		 * 
		 * @param	evt
		 * Event handle
		 */
		private function OnLevelLoad( evt:Event ):void
		{
			// Load level XML
			LoadLevelXML( new XML( uploadFile.data ) );
			
			uploadFile = null;
		}
		
		/**
		 * An image file has been uploaded, import it
		 * 
		 * @param	evt
		 * Event handle
		 */
		private function OnImageLoad( evt:Event ):void
		{
			// Import image data
			layerSources.push( uploadFile.data );
			
			var imageLayer:Entity = add( new Entity() );
			imageLayer.layer = BACKGROUND_LAYER;
			imageLayers.push( imageLayer );
			new ImageLoader( imageLayer, uploadFile.data );
			
			// Place background image
			state = STATE_PLACING;
			placeObject = add( new PlaceObject( imageLayer, EndState ) ) as PlaceObject;
			
			uploadFile = null;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the game state
		 */
		public function get state():int
		{
			return _state;
		}
		
		/**
		 * Gets or sets the game state
		 */
		public function set state( value:int ):void
		{
			// End the current state
			switch( _state )
			{
				case STATE_DRAWING:
					remove( drawLines );
					drawLines = null;
					break;
				case STATE_ERASING:
					remove( eraser );
					eraser = null;
					break;
				case STATE_PLACING:
					remove( placeObject );
					placeObject = null;
					break;
				case STATE_DANGER:
					remove( drawLines );
					drawLines = null;
					break;
				case STATE_LAYER_LIST:
					remove( layerList );
					layerList = null;
					break;
				case STATE_PROPERTIES:
					remove( propertyPane );
					propertyPane = null;
					break;
			}
			
			_state = value;
			
			// Start the new state
			switch( _state )
			{
				case STATE_NONE:
					gameState.text = ResourceManager.getInstance().getString( "resources", "GAME_STATE_NONE" )
					break;
				case STATE_ERASING:
					gameState.text = ResourceManager.getInstance().getString( "resources", "GAME_STATE_ERASING" )
					eraser = add( new Eraser() ) as Eraser;
					break;
				case STATE_DRAWING:
					gameState.text = ResourceManager.getInstance().getString( "resources", "GAME_STATE_DRAWING" )
					drawLines = add( new DrawLines() ) as DrawLines;
					break;
				case STATE_DANGER:
					gameState.text = ResourceManager.getInstance().getString( "resources", "GAME_STATE_DRAWING" )
					drawLines = add( new DrawLines( null, true ) ) as DrawLines;
					break;
				case STATE_PLACING:
					gameState.text = ResourceManager.getInstance().getString( "resources", "GAME_STATE_PLACING" )
					break;
				case STATE_LAYER_LIST:
					layerList = add( new ImageLayerList() ) as ImageLayerList;
					break;
				case STATE_PROPERTIES:
					break;
			}
		}
		
		/**
		 * Gets or sets the outdoor environment flag - set to true to display
		 * the air counter.
		 */
		public function get outdoor():Boolean
		{
			return _outdoor;
		}
		
		/**
		 * Gets or sets the outdoor environment flag - set to true to display
		 * the air counter.
		 */
		public function set outdoor( value:Boolean ):void
		{
			_outdoor = value;
			player.doubleJumpEnabled = realOutdoor;
			
			// Create or delete the air timer as necessary
			airLeft.SetEnvironment( value );
		}
		
		/**
		 * Set to true to show edit handles for all objects, false to hide
		 */
		public function get showHandles():Boolean
		{
			return _showHandles;
		}
		
		/**
		 * Set to true to show edit handles for all objects, false to hide
		 */
		public function set showHandles( value:Boolean ):void
		{
			_showHandles = value;
			
			var objects:Vector.<IEditHandle> = new Vector.<IEditHandle>();
			getClass( IEditHandle, objects );
			for each( var object:IEditHandle in objects )
			{
				if ( _showHandles )
					object.ShowHandle();
				else
					object.HideHandle();
			}
		}
	}

}