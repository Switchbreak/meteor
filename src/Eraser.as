package  
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.utils.Input;
	import net.flashpunk.FP;
	import Interfaces.*;
	
	/**
	 * Eraser entity
	 * 
	 * @author Switchbreak
	 */
	public class Eraser extends Entity 
	{
		
		// Constants
		private static const ERASE_RADIUS:Number = 10;
		private static const ERASE_RADIUS_SQUARED:Number = ERASE_RADIUS * ERASE_RADIUS;
		private static const ERASE_COLOR:uint = 0xFFFFFF;
		private static const ERASE_ALPHA:Number = 0.5;
		
		// Graphics
		private var cursor:Image;
		
		/**
		 * Create and initialize the eraser entity
		 */
		public function Eraser() 
		{
			// Create graphic
			cursor = Image.createCircle( ERASE_RADIUS, ERASE_COLOR, ERASE_ALPHA );
			cursor.centerOrigin();
			super( 0, 0, cursor );
			visible = false;
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Do erasing when the mouse button is pressed
			if ( Input.mouseDown )
			{
				x = world.mouseX;
				y = world.mouseY;
				visible = true;
				Input.mouseCursor = "hide";
				
				// Find any lines within radius and erase them
				var lines:Vector.<Line> = GameWorld.currentInstance.collisionLines;
				for ( var i:int = 0; i < lines.length; ++i )
				{
					if ( lines[i].DistanceToLine( new Point( x, y ), ERASE_RADIUS_SQUARED ) )
					{
						if ( lines[i].parent == null )
						{
							GameWorld.currentInstance.collisionLines.splice( i, 1 );
							i--;
						}
						else
						{
							world.remove( lines[i].parent );
						}
					}
				}
					
				// Delete entities if edit handles are shown
				if ( GameWorld.currentInstance.showHandles )
				{
					var objects:Vector.<Entity> = new Vector.<Entity>();
					world.getClass( ISavable, objects );
					for each( var object:Entity in objects )
					{
						if ( object.distanceToPoint( x, y, true ) < ERASE_RADIUS )
						{
							world.remove( object );
						}
					}
				}
			}
			else
			{
				visible = false;
				Input.mouseCursor = "auto";
			}
			
			super.update();
		}
	}

}