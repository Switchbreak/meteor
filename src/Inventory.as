package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	
	/**
	 * Inventory object, tracks and displays player inventory
	 * 
	 * @author Switchbreak
	 */
	public class Inventory
	{
		// Constants
		private static const PADDING:int = 5;
		
		// Private variables
		public var objects:Vector.<Entity> = new Vector.<Entity>();
		
		/**
		 * Add an object to the inventory
		 * 
		 * @param	object
		 */
		public function AddObject( object:Entity ):void
		{
			object.layer = GameWorld.HUD_LAYER;
			object.graphic.scrollX = 0;
			object.graphic.scrollY = 0;
			objects.push( object );
			UpdateDisplay();
		}
		
		/**
		 * Remove an object from the inventory
		 */
		public function RemoveObject( object:Entity ):void
		{
			FP.world.remove( object );
			objects.splice( objects.indexOf( object ), 1 );
			UpdateDisplay();
		}
		
		/**
		 * Sets the position of all objects in inventory to display on the upper
		 * right side of the screen
		 */
		private function UpdateDisplay():void
		{
			var position:int = FP.width - PADDING;
			for each( var object:Entity in objects )
			{
				position -= object.width;
				object.x = position;
				object.y = PADDING;
			}
		}
	}

}