package GameObjects
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.utils.*;
	import Interfaces.*;
	
	/**
	 * Elevator game object entity
	 * 
	 * @author Switchbreak
	 */
	public class Elevator extends Entity 
		implements IActivatable, ITriggerable, ISavable
	{
		// Embeds
		[Embed(source = "../../snd/elevator.mp3")] private static const ELEVATOR_SND:Class;
		
		// Constants
		private static const ELEVATOR_WIDTH:int = 30;
		private static const ACTIVATE_HEIGHT:int = 10;
		private static const MOVE_DURATION:Number = 1.0;
		private static const TRACK_HEIGHT:int = 103;
		private static const EPSILON:Number = 0.000001;
		private static const HOVER_X:int = 10;
		private static const HOVER_Y:int = -38;
		private static const ELEVATOR_COLOR:uint = 0x00ff00;
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Lift"] );
		private static const PROPERTIES:Vector.<String> = null;
		
		// Private variables
		private var placing:Boolean;
		private var _activated:Boolean = false;
		private var moveTween:NumTween;
		private var up:Boolean = false;
		
		// Graphics
		private var collisionLine1:Line;
		private var collisionLine2:Line;
		private var hoverText:Text;
		
		// Objects
		private var elevatorSfx:Sfx = new Sfx( ELEVATOR_SND );
		
		/**
		 * Create and initialize the elevator object
		 */
		public function Elevator( startPosition:Point = null ) 
		{
			// Create hovertext
			hoverText = new Text( "X", HOVER_X, HOVER_Y );
			hoverText.visible = false;
			graphic = hoverText;
			
			// Initialize collision objects
			moveTween = new NumTween(null, ONESHOT);
			collisionLine1 = new Line( new Point(), new Point() );
			collisionLine1.parent = this;
			collisionLine2 = new Line( collisionLine1.end2, collisionLine1.end1 );
			collisionLine2.parent = this;
			type = GameWorld.ACTIVATABLE_TYPE;
			setHitbox( ELEVATOR_WIDTH, ACTIVATE_HEIGHT, 0, ACTIVATE_HEIGHT );
			
			// Choose set starting position or level edit mode
			if ( startPosition != null )
			{
				SetPosition( startPosition.x, startPosition.y );
				placing = false;
			}
			else
			{
				placing = true;
			}
		}
		
		/**
		 * Create child objects when added to the world
		 */
		override public function added():void 
		{
			(world as GameWorld).collisionLines.push( collisionLine1 );
			(world as GameWorld).collisionLines.push( collisionLine2 );
		}
		
		/**
		 * Cleanup when object is removed
		 */
		override public function removed():void 
		{
			(world as GameWorld).collisionLines.splice( GameWorld.currentInstance.collisionLines.indexOf( collisionLine1 ), 1 );
			(world as GameWorld).collisionLines.splice( GameWorld.currentInstance.collisionLines.indexOf( collisionLine2 ), 1 );
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			if ( placing )
			{
				// Set position
				SetPosition( world.mouseX, world.mouseY );
				
				// Place elevator down when mouse button is clicked
				if ( Input.mousePressed )
				{
					placing = false;
				}
			}
			else if ( moveTween.active )
			{
				// Elevator is moving
				SetPosition( x, moveTween.value );
			}
			else if( _activated )
			{
				if ( Input.check( Key.X ) && GameWorld.currentInstance.player.onGround )
				{
					MoveLift();
				}
			}
			
			// Show hovertext when player stands on the elevator
			hoverText.visible = _activated && !moveTween.active && GameWorld.currentInstance.player.onGround;
			
			// Attach player to elevator if colliding with it and not jumping
			// This is a workaround for physics not working with moving objects
			if ( _activated && GameWorld.currentInstance.player.onGround && GameWorld.currentInstance.player.x > x - Player.PLAYER_WIDTH && GameWorld.currentInstance.player.x < x + ELEVATOR_WIDTH )
			{
				GameWorld.currentInstance.player.y = y - EPSILON - Player.PLAYER_HEIGHT;
			}
			
			super.update();
		}
		
		override public function render():void 
		{
			Draw.line( collisionLine1.end1.x, collisionLine1.end1.y, collisionLine1.end2.x, collisionLine1.end2.y, ELEVATOR_COLOR );
			
			super.render();
		}
		
		/**
		 * Set the elevator position
		 * 
		 * @param	setX
		 * Position to move the elevator to
		 * @param	setY
		 * Position to move the elevator to
		 */
		private function SetPosition( setX:Number, setY:Number ):void
		{
			x = setX;
			y = setY;
			collisionLine1.end1.x = setX;
			collisionLine1.end1.y = setY;
			collisionLine1.end2.x = setX + ELEVATOR_WIDTH;
			collisionLine1.end2.y = setY;
		}
		
		/**
		 * Move the lift up and down if it isn't already moving
		 */
		private function MoveLift():void
		{
			if ( !moveTween.active )
			{
				elevatorSfx.play( 0.8 );
				moveTween.tween( y, (up ? y + TRACK_HEIGHT : y - TRACK_HEIGHT), MOVE_DURATION, (up ? Ease.quadOut : Ease.backOut) );
				addTween( moveTween, true );
				up = !up;
			}
		}
		
		/**
		 * Trigger the elevator
		 */
		public function Trigger( event:String, value:int ):void
		{
			MoveLift();
		}
		
		/**
		 * Return the elevator as an XML object for saving
		 * 
		 * @return
		 * Serialized elevator object
		 */
		public function GetSaveObject():XML
		{
			return <elevator x={x} y={y} name={name} />;
		}
		
		/**
		 * Load elevator from an XML object
		 * 
		 * @param	node
		 * XML object containing elevator definition
		 * @return
		 * Newly created elevator object
		 */
		public static function Deserialize( node:XML ):Elevator
		{
			var elevator:Elevator = new Elevator( new Point( node.@x, node.@y ) );
			if ( node.@name != null )
				elevator.name = node.@name;
			
			return elevator;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function get activated():Boolean
		{
			return _activated;
		}
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function set activated( value:Boolean ):void
		{
			_activated = value;
		}
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
	}

}