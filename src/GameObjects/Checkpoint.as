package GameObjects
{
	import Interfaces.IActivatable;
	import Interfaces.ICollisionCircle;
	import Interfaces.ISavable;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.utils.Draw;
	
	/**
	 * Oxygen tank game object
	 * 
	 * @author Switchbreak
	 */
	public class Checkpoint extends Entity implements ICollisionCircle, ISavable 
	{
		// Embeds
		[Embed(source = "../../img/Checkpoint.png")] private static const CHECKPOINT:Class;
		[Embed(source="../../img/OxygenGenerator.png")] private static const GENERATOR:Class;
		
		// Constants
		private static const OXYGEN_AMOUNT:Number = 20;
		private static const BUBBLE_RADIUS:Number = 60;
		private static const BUBBLE_COLOR:uint = 0x00ffff;
		private static const CHECKPOINT_EVENT:String = "Checkpoint";
		private static const PROPERTIES:Vector.<String> = null;
		
		// Private variables
		private var _activated:Boolean = false;
		private var _radius:Number;
		private var checkpointStarted:Boolean = false;
		
		// Graphics
		private var generator:Image;
		
		/**
		 * Create and initialize the game object
		 */
		public function Checkpoint( setX:int = 0, setY:int = 0 )
		{
			// Create graphic
			generator = new Image( CHECKPOINT );
			generator.centerOrigin();
			_radius = generator.width >> 1;
			
			super( setX, setY, generator );
			type = GameWorld.ACTIVATABLE_TYPE;
			setHitbox( generator.width, generator.height, radius, radius );
			
			// Set object values
			active = false;
		}
		
		/**
		 * Draw the oxygen bubble
		 */
		override public function render():void 
		{
			if( checkpointStarted )
				Draw.circle( x, y, _radius, BUBBLE_COLOR );
			
			super.render();
		}
		
		/**
		 * Return the oxygen tank as an XML object for saving
		 * 
		 * @return
		 * Serialized key object
		 */
		public function GetSaveObject():XML
		{
			return <checkpoint x={x} y={y} />;
		}
		
		/**
		 * Load oxygen tank from an XML object
		 * 
		 * @param	node
		 * XML object containing oxygen tank definition
		 * @return
		 * Newly created oxygen tank object
		 */
		public static function Deserialize( node:XML ):Checkpoint
		{
			var checkpoint:Checkpoint = new Checkpoint( node.@x, node.@y );
			if ( node.@name != null )
				checkpoint.name = node.@name;
			
			return checkpoint;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function get activated():Boolean
		{
			return _activated;
		}
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function set activated( value:Boolean ):void
		{
			_activated = value;
			active = value;
			
			if ( !checkpointStarted && value )
			{
				generator = new Image( GENERATOR );
				generator.centerOrigin();
				graphic = generator;
				
				GameWorld.TriggerEvent( CHECKPOINT_EVENT );
				
				_radius = BUBBLE_RADIUS;
				checkpointStarted = true;
				setHitbox( _radius << 1, _radius << 1, _radius, _radius );
			}
			
			if ( _activated )
			{
				GameWorld.currentInstance.outdoor = false;
			}
			else
			{
				GameWorld.currentInstance.outdoor = GameWorld.currentInstance.realOutdoor;
			}
		}
		
		/**
		 * Returns collision radius
		 */
		public function get radius():Number
		{
			return _radius;
		}
	}

}