package GameObjects
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.utils.*;
	import Interfaces.*;
	
	/**
	 * Elevator game object entity
	 * 
	 * @author Switchbreak
	 */
	public class Button extends Entity 
		implements IActivatable, ISavable, IEditHandle, IEventFirable
	{
		// Constants
		private static const ACTIVATE_WIDTH:int = 10;
		private static const ACTIVATE_HEIGHT:int = 10;
		private static const EDIT_HANDLE_COLOR:uint = 0x00ff00;
		private static const EDIT_HANDLE_ALPHA:Number = 0.5;
		private static const MOVE_DURATION:Number = 1.0;
		private static const TRACK_HEIGHT:int = 100;
		private static const EPSILON:Number = 0.000001;
		private static const HOVER_X:int = 0;
		private static const HOVER_Y:int = -20;
		private static const PRESS_EVENT:int = 0;
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["press"] );
		private static const PROPERTIES:Vector.<String> = null;
		
		// Private variables
		private var placing:Boolean = false;
		private var _activated:Boolean = false;
		
		// Public variables
		public var _target:EventTargeter;
		
		// Graphics
		private var hoverText:Text;
		private var editHandle:Image = null;
		
		/**
		 * Create and initialize the elevator object
		 */
		public function Button( startPosition:Point = null ) 
		{
			// Create hovertext
			hoverText = new Text( "X", HOVER_X, HOVER_Y );
			hoverText.visible = false;
			addGraphic( hoverText );
			
			// Initialize collision objects
			type = GameWorld.ACTIVATABLE_TYPE;
			_target = new EventTargeter();
			setHitbox( ACTIVATE_WIDTH, ACTIVATE_HEIGHT, 0, 0 );
			
			// Choose set starting position or level edit mode
			if ( startPosition == null )
			{
				ShowHandle();
				placing = true;
			}
			else
			{
				x = startPosition.x;
				y = startPosition.y;
			}
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			if ( placing )
			{
				// Set position
				x = world.mouseX;
				y = world.mouseY;
				
				// Place elevator down when mouse button is clicked
				if ( Input.mousePressed )
				{
					HideHandle();
					placing = false;
				}
			}
			else if( _activated )
			{
				if ( Input.pressed( Key.X ) )
					_target.FireEvent( TRIGGER_EVENTS[PRESS_EVENT], 1 );
			}
			
			super.update();
		}
		
		/**
		 * Draw event lines if edit handles are showing
		 */
		override public function render():void 
		{
			if ( editHandle )
				_target.DrawEventLines( x + halfWidth, y + halfHeight );
			
			super.render();
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			if ( editHandle == null )
			{
				editHandle = Image.createRect( ACTIVATE_WIDTH, ACTIVATE_HEIGHT, EDIT_HANDLE_COLOR, EDIT_HANDLE_ALPHA );
				addGraphic( editHandle );
			}
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			if ( editHandle != null )
			{
				(graphic as Graphiclist).remove( editHandle );
				editHandle = null;
			}
		}
		
		/**
		 * Return the button as an XML object for saving
		 * 
		 * @return
		 * Serialized elevator object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <button x={x} y={y} />;
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load button from an XML object
		 * 
		 * @param	buttonXML
		 * XML object containing button definition
		 * @return
		 * Newly created button object
		 */
		public static function Deserialize( buttonXML:XML ):Button
		{
			var button:Button = new Button( new Point( buttonXML.@x, buttonXML.@y ) );
			
			button._target = new EventTargeter( buttonXML );
			if ( buttonXML.@name != null )
				button.name = buttonXML.@name;
			
			return button;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function get activated():Boolean
		{
			return _activated;
		}
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function set activated( value:Boolean ):void
		{
			_activated = value;
			hoverText.visible = _activated
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}
	}

}