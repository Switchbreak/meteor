package GameObjects 
{
	import Interfaces.*;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Spritemap;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import flash.geom.Point;
	import mx.resources.ResourceManager;
	
	/**
	 * Cat game object
	 * 
	 * @author Switchbreak
	 */
	public class Cat extends Entity implements ISavable, IMovable, IActivatable, IEventFirable
	{
		// Embeds
		[Embed(source = "../../img/CatSprite.png")] private static const CAT_SPRITE:Class;
		
		// Constants
		private static const CAT_WIDTH:int = 28;
		private static const CAT_HEIGHT:int = 20;
		private static const WALK_ANIM:String = "walk";
		private static const IDLE_ANIM:String = "idle";
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["pet"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const PET_EVENT:int = 0;
		private static const WALK_SPEED:Number = 25;
		private static const GRAVITY:Number = 1300;
		private static const MAX_COLLISION_RESOLVES:int = 200;
		private static const WALL_MIN_SLOPE:int = 1.73205081;					// 60 degree angle
		private static const EPSILON:Number = 0.000001;
		private static const HOVER_X:int = 10;
		private static const HOVER_Y:int = -20;
		private static const MEOW_TIME:Number = 1;
		
		// Line types
		private static const TYPE_FLOOR:int = 0;
		private static const TYPE_CEILING:int = 1;
		private static const TYPE_LEFT_WALL:int = 2;
		private static const TYPE_RIGHT_WALL:int = 3;
		
		// Public variables
		public var _target:EventTargeter;
		public var onGround:Boolean = false;
		public var velocityX:int = 0;
		public var velocityY:int = 0;
		public var accelerationX:int = 0;
		public var accelerationY:int = GRAVITY;
		
		// Private variables
		private var destX:Number;
		private var walkingDirection:int = 0;
		private var moveCallback:Function;
		private var _activated:Boolean = false;
		private var meowTimer:Number = 0;
		
		// Graphics
		private var catSprite:Spritemap;
		private var hoverText:Text;
		private var meowText:Text;
		
		/**
		 * Create and initialize the cat object
		 */
		public function Cat( setX:int = 0, setY:int = 0 ) 
		{
			// Create the cat sprite graphic and set up animations
			catSprite = new Spritemap( CAT_SPRITE, CAT_WIDTH, CAT_HEIGHT );
			catSprite.add( WALK_ANIM, [0, 1, 2], 5, true );
			catSprite.add( IDLE_ANIM, [1], 0, false );
			addGraphic( catSprite );
			
			// Create hovertext
			hoverText = new Text( "X", HOVER_X, HOVER_Y );
			hoverText.visible = false;
			addGraphic( hoverText );
			
			// Create hovertext
			meowText = new Text( ResourceManager.getInstance().getString( "resources", "MEOW" ) );
			meowText.visible = false;
			addGraphic( meowText );
			
			type = GameWorld.ACTIVATABLE_TYPE;
			_target = new EventTargeter();
			super( setX, setY );
			setHitboxTo( catSprite );
			
			catSprite.play( IDLE_ANIM );
		}
		
		/**
		 * Move to a destination point immediately
		 * 
		 * @param	destPoint
		 * Position to warp to
		 */
		public function WarpTo( destPoint:Point ):void
		{
			x = destPoint.x - halfWidth;
			y = destPoint.y - halfHeight;
		}
		
		/**
		 * Move to a destination point at your own speed
		 * 
		 * @param	destPoint
		 * Position to move to
		 * @param	completeCallback
		 * Function to call when the move is complete
		 */
		public function MoveTo( destPoint:Point, completeCallback:Function ):void
		{
			destX = destPoint.x - halfWidth;
			moveCallback = completeCallback;
			
			if ( destX < x )
			{
				catSprite.flipped = false;
				walkingDirection = -1;
			}
			else
			{
				catSprite.flipped = true;
				walkingDirection = 1;
			}
			catSprite.play( WALK_ANIM, true );
		}
		
		/**
		 * Handle movement
		 */
		override public function update():void 
		{
			if ( walkingDirection != 0 )
			{
				if ( (walkingDirection > 0 && x > destX) || (walkingDirection < 0 && x < destX) )
				{
					velocityX = 0;
					walkingDirection = 0;
					catSprite.play( IDLE_ANIM, true );
					moveCallback();
				}
				else
					velocityX = WALK_SPEED * walkingDirection;
			}
			
			IntegrateMotion();
			
			if ( meowTimer > 0 )
			{
				meowTimer -= FP.elapsed;
				if ( meowTimer <= 0 )
				{
					meowTimer = 0;
					meowText.visible = false;
				}
			}
			
			// Handle input
			if( _activated && Input.pressed( Key.X ) )
			{
				meowText.x = width;
				meowText.y = halfHeight - (meowText.height >> 1);
				meowText.visible = true;
				meowTimer = MEOW_TIME;
				
				_target.FireEvent( TRIGGER_EVENTS[PET_EVENT], 1 );
			}
			
			super.update();
		}
		
		/**
		 * Integrate velocity and acceleration into player motion, and handle
		 * collision detection
		 */
		private function IntegrateMotion():void
		{
			// Integrate acceleration and move object by velocity vector
			var integrateX:Number = (accelerationX >> 1) * FP.elapsed;
			var integrateY:Number = (accelerationY >> 1) * FP.elapsed;
			
			velocityX += integrateX;
			velocityY += integrateY;
			
			// Move with collision detection
			CollisionDetect( x + velocityX * FP.elapsed, y + velocityY * FP.elapsed );
			
			velocityX += integrateX;
			velocityY += integrateY;
		}
		
		/**
		 * Move to a destination point and check for collisions on the way
		 * 
		 * @param	toX
		 * Destination point
		 * @param	toY
		 * Destination point
		 */
		private function CollisionDetect( toX:Number, toY:Number ):void
		{
			// Check that the player moved
			if ( x == toX && y == toY )
				return;
			
			// Create collision points for the four corners of the hitbox
			var from:Vector.<Point> = new Vector.<Point>(4);
			from[0] = new Point( x, y );
			from[1] = new Point( x + CAT_WIDTH, y );
			from[2] = new Point( x, y + CAT_HEIGHT );
			from[3] = new Point( x + CAT_WIDTH, y + CAT_HEIGHT );
			
			// Create destination points for the four corners of the hitbox
			var dest:Vector.<Point> = new Vector.<Point>(4);
			dest[0] = new Point( toX, toY );
			dest[1] = new Point( toX + CAT_WIDTH, toY );
			dest[2] = new Point( toX, toY + CAT_HEIGHT );
			dest[3] = new Point( toX + CAT_WIDTH, toY + CAT_HEIGHT );
			
			var points:Vector.<int> = new Vector.<int>();
			var leavingGround:Boolean = onGround;
			onGround = false;
			var collided:Boolean = true;
			var resolveCount:int = 0;
			
			// Get all lines in the world
			var lines:Vector.<Line> = GameWorld.currentInstance.collisionLines;
			
			// Continue checking until there are no collisions to resolve
			while ( collided )
			{
				if ( resolveCount > MAX_COLLISION_RESOLVES )
					break;
				
				resolveCount++;
				collided = false;
				
				// Check each line for collision
				for each( var line:Line in lines )
				{
					// Check that the line exists
					if ( line.end1.x == line.end2.x && line.end1.y == line.end2.y )
						continue;
					
					// Boundary check the motion line against the collision line
					if ( Math.min( from[0].x, dest[0].x ) <= Math.max( line.end1.x, line.end2.x )
						&& Math.max( from[3].x, dest[3].x ) >= Math.min( line.end1.x, line.end2.x )
						&& Math.min( from[0].y, dest[0].y ) <= Math.max( line.end1.y, line.end2.y )
						&& Math.max( from[3].y, dest[3].y ) >= Math.min( line.end1.y, line.end2.y ) )
					{
						// Determine line type
						var lineType:int;
						points.length = 0;
						if ( line.end1.x == line.end2.x || Math.abs( (line.end2.y - line.end1.y) / (line.end2.x - line.end1.x) ) > WALL_MIN_SLOPE )
						{
							if ( line.end2.y > line.end1.y )
							{
								lineType = TYPE_LEFT_WALL;
								points.push( 0, 2 );
							}
							else
							{
								lineType = TYPE_RIGHT_WALL;
								points.push( 1, 3 );
							}
						}
						else
						{
							if ( line.end2.x > line.end1.x )
							{
								lineType = TYPE_FLOOR;
								points.push( 2, 3 );
							}
							else
							{
								lineType = TYPE_CEILING;
								points.push( 0, 1 );
							}
						}
						
						for ( var i:int = 0; i < points.length; ++i )
						{
							collided = ResolveCollision( from[points[i]], dest[points[i]], line, lineType );
							if ( collided )
							{
								var diffX:Number = dest[points[i]].x - from[points[i]].x;
								var diffY:Number = dest[points[i]].y - from[points[i]].y;
								
								for ( var j:int = 0; j < 4; ++j )
								{
									dest[j].x = from[j].x + diffX;
									dest[j].y = from[j].y + diffY;
								}
								
								break;
							}
						}
						
						if ( collided )
							break;
					}
				}
			}
			
			// Move to new position
			x = dest[0].x;
			y = dest[0].y;
		}
		
		/**
		 * Resolve a collision with a line
		 * 
		 * @param	dest
		 * @param	line
		 */
		private function ResolveCollision( from:Point, dest:Point, line:Line, lineType:int ):Boolean
		{
			// Get the following vectors:
			// - a: vector from line point 1 to line point 2
			// - b: bector from line point 1 to motion start point
			// - c: vector from line point 1 to motion end point
			var a:Point = new Point( line.end2.x - line.end1.x, line.end2.y - line.end1.y );
			var b:Point = new Point( from.x - line.end1.x, from.y - line.end1.y );
			var c:Point = new Point( dest.x - line.end1.x, dest.y - line.end1.y );
			
			// Get scalar projection of b and c onto a's orthogonal
			// vector, use the signs to determine which side of a
			// the motion start and end points fall on
			var o1:Number = (a.x * b.y - a.y * b.x);
			var o2:Number = (a.x * c.y - a.y * c.x);
			
			// Line collision is one way, only capture collisions
			// where motion start is left of the line and motion end
			// is right
			if ( o1 < 0 && o2 > 0 )
			{
				// Get the intersection point on a
				var v:Number = ((dest.x - from.x) * b.y - (dest.y - from.y) * b.x) / (a.y * (dest.x - from.x) - a.x * (dest.y - from.y));
				
				// If the intersection point is inside the line
				// segment, the lines intersect
				if ( v >= 0 && v <= 1 )
				{
					// Workaround - if X velocity is zero, then
					// "stick" to the surface and don't project a
					// new position - this allows sliding along the
					// ground when moving horizontally, but stops
					// sliding along slopes due to gravity
					if ( lineType == TYPE_LEFT_WALL || lineType == TYPE_RIGHT_WALL || Math.abs( velocityX ) > EPSILON )
					{
						// Project target point onto line
						v = (a.x * c.x + a.y * c.y) / (a.x * a.x + a.y * a.y)
					}
					
					// Set new target point on the line at scalar
					// position v
					dest.x = line.end1.x + a.x * v;
					dest.y = line.end1.y + a.y * v;
					
					if ( lineType == TYPE_FLOOR )
					{
						velocityY = 0;
						onGround = true;
						
						// Move point up by EPSILON to avoid the point
						// being congruent with the line
						dest.y -= EPSILON;
					}
					else if ( lineType == TYPE_CEILING )
					{
						velocityY = Math.max( velocityY, 0 );
						dest.y += EPSILON;
					}
					else if ( lineType == TYPE_LEFT_WALL )
					{
						velocityX = 0;
						dest.x += EPSILON;
					}
					else if( lineType == TYPE_RIGHT_WALL )
					{
						velocityX = 0;
						dest.x -= EPSILON;
					}
					
					// Set collision flag
					return true;
				}
			}
			
			return false;
		}
		
		/**
		 * Return the key as an XML object for saving
		 * 
		 * @return
		 * Serialized key object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <cat x={x} y={y} name={name} />;
			
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load key from an XML object
		 * 
		 * @param	node
		 * XML object containing key definition
		 * @return
		 * Newly created key object
		 */
		public static function Deserialize( node:XML ):Cat
		{
			var cat:Cat = new Cat( node.@x, node.@y );
			
			cat._target = new EventTargeter( node );
			if ( node.@name != null )
				cat.name = node.@name;
			
			return cat;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function get activated():Boolean
		{
			return _activated;
		}
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function set activated( value:Boolean ):void
		{
			_activated = value;
			hoverText.visible = _activated
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}
	}
}