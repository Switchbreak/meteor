package GameObjects
{
	import Interfaces.IActivatable;
	import Interfaces.ICollisionCircle;
	import Interfaces.ISavable;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.FP;
	import net.flashpunk.Sfx;
	import net.flashpunk.utils.Draw;
	
	/**
	 * Oxygen tank game object
	 * 
	 * @author Switchbreak
	 */
	public class OxygenGenerator extends Entity implements ICollisionCircle, ISavable 
	{
		// Embeds
		[Embed(source="../../img/OxygenGenerator.png")] private static const GENERATOR:Class;
		
		// Constants
		private static const OXYGEN_AMOUNT:Number = 20;
		private static const BUBBLE_RADIUS:Number = 60;
		private static const BUBBLE_COLOR:uint = 0x00ffff;
		private static const PROPERTIES:Vector.<String> = null;
		
		// Private variables
		private var _activated:Boolean = false;
		private var airLeft:Number = OXYGEN_AMOUNT;
		private var _radius:Number = BUBBLE_RADIUS;
		
		// Graphics
		private var generator:Image;
		
		/**
		 * Create and initialize the game object
		 */
		public function OxygenGenerator( setX:int = 0, setY:int = 0 )
		{
			// Create graphic
			generator = new Image( GENERATOR );
			generator.centerOrigin();
			super( setX, setY, generator );
			type = GameWorld.ACTIVATABLE_TYPE;
			setHitbox( BUBBLE_RADIUS << 1, BUBBLE_RADIUS << 1, BUBBLE_RADIUS, BUBBLE_RADIUS );
			
			// Set object values
			active = false;
		}
		
		/**
		 * Handle oxygen draining
		 */
		override public function update():void 
		{
			if ( _activated )
			{
				airLeft -= AirTimer.RATE * FP.elapsed;
				if ( airLeft <= 0 )
				{
					airLeft = 0;
					type = null;
				}
				_radius = airLeft * BUBBLE_RADIUS / OXYGEN_AMOUNT;
			}
			
			super.update();
		}
		
		/**
		 * Draw the oxygen bubble
		 */
		override public function render():void 
		{
			Draw.circle( x, y, _radius, BUBBLE_COLOR );
			
			super.render();
		}
		
		/**
		 * Return the oxygen tank as an XML object for saving
		 * 
		 * @return
		 * Serialized key object
		 */
		public function GetSaveObject():XML
		{
			return <tank x={x} y={y} />;
		}
		
		/**
		 * Load oxygen tank from an XML object
		 * 
		 * @param	node
		 * XML object containing oxygen tank definition
		 * @return
		 * Newly created oxygen tank object
		 */
		public static function Deserialize( node:XML ):OxygenGenerator
		{
			var tank:OxygenGenerator = new OxygenGenerator( node.@x, node.@y );
			if ( node.@name != null )
				tank.name = node.@name;
			
			return tank;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function get activated():Boolean
		{
			return _activated;
		}
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function set activated( value:Boolean ):void
		{
			_activated = value;
			active = value;
			
			if ( _activated )
			{
				GameWorld.currentInstance.outdoor = false;
			}
			else
			{
				GameWorld.currentInstance.outdoor = GameWorld.currentInstance.realOutdoor;
			}
		}
		
		/**
		 * Returns collision radius
		 */
		public function get radius():Number
		{
			return _radius;
		}
	}

}