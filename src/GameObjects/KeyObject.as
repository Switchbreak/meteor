package GameObjects
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.FP;
	import Interfaces.*;
	
	/**
	 * Key game object
	 * 
	 * @author Switchbreak
	 */
	public class KeyObject extends Entity
		implements IActivatable, ISavable
	{
		// Embeds
		[Embed(source = "../../img/Key.png")] private static const KEY:Class;
		
		// Constants
		private static const PROPERTIES:Vector.<String> = null;
		
		/**
		 * Create and initialize the key object
		 * 
		 * @param	setX
		 * Starting position of the key
		 * @param	setY
		 * Starting position of the key
		 */
		public function KeyObject( setX:int = 0, setY:int = 0 ) 
		{
			// Set object values
			type = GameWorld.ACTIVATABLE_TYPE;
			active = false;
			
			// Create the object
			super( setX, setY, new Stamp( KEY ) );
			setHitboxTo( graphic );
		}
		
		/**
		 * Return the key as an XML object for saving
		 * 
		 * @return
		 * Serialized key object
		 */
		public function GetSaveObject():XML
		{
			return <key x={x} y={y} />;
		}
		
		/**
		 * Load key from an XML object
		 * 
		 * @param	node
		 * XML object containing key definition
		 * @return
		 * Newly created key object
		 */
		public static function Deserialize( node:XML ):KeyObject
		{
			var key:KeyObject = new KeyObject( node.@x, node.@y );
			if ( node.@name != null )
				key.name = node.@name;
			
			return key;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function get activated():Boolean
		{
			return false;
		}
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function set activated( value:Boolean ):void
		{
			// If value is true, player has collided with the key and 
			if ( value == true )
			{
				// Remove collision type
				type = null;
				GameWorld.currentInstance.inventory.AddObject( this );
			}
		}
	}

}