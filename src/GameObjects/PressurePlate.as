package GameObjects
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.tweens.misc.NumTween;
	import net.flashpunk.utils.*;
	import Interfaces.*;
	
	/**
	 * Elevator game object entity
	 * 
	 * @author Switchbreak
	 */
	public class PressurePlate extends Entity 
		implements IActivatable, ISavable, IEditHandle, IEventFirable
	{
		// Constants
		private static const EDIT_HANDLE_COLOR:uint = 0x00ff00;
		private static const EDIT_HANDLE_ALPHA:Number = 0.5;
		private static const MOVE_DURATION:Number = 1.0;
		private static const TRACK_HEIGHT:int = 100;
		private static const EPSILON:Number = 0.000001;
		private static const HOVER_X:int = 0;
		private static const HOVER_Y:int = -20;
		private static const PRESS_EVENT:int = 0;
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["press"] );
		private static const PROPERTIES:Vector.<String> = null;
		
		// Private variables
		private var placing:int = 0;
		private var _activated:Boolean = false;
		private var showHandle:Boolean = false;
		
		// Public variables
		public var _target:EventTargeter;
		
		/**
		 * Create and initialize the elevator object
		 */
		public function PressurePlate( startPosition:Point = null, setWidth:int = 0, setHeight:int = 0 ) 
		{
			// Set object values
			active = false;
			_target = new EventTargeter();
			
			// Initialize collision objects
			type = GameWorld.ACTIVATABLE_TYPE;
			setHitbox( setWidth, setHeight, 0, 0 );
			
			// Choose set starting position or level edit mode
			if ( startPosition == null )
			{
				ShowHandle();
				placing = 1;
				active = true;
			}
			else
			{
				x = startPosition.x;
				y = startPosition.y;
			}
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Handle input for edit mode
			if ( placing > 0 )
			{
				// Size the handle box when placing the second point
				if ( placing == 2 )
				{
					setHitbox( world.mouseX - x, world.mouseY - y );
				}
				
				// Set corner on mouse down
				if ( Input.mousePressed )
				{
					// Set whichever corner we are currently placing
					if ( placing == 1 )
					{
						x = world.mouseX;
						y = world.mouseY;
						placing = 2;
					}
					else
					{
						if ( width < 0 )
						{
							x += width;
							width = -width;
						}
						if ( height < 0 )
						{
							y += height;
							height = -height;
						}
						
						placing = 0;
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Show the edit handle if enabled
		 */
		override public function render():void 
		{
			if ( showHandle )
			{
				Draw.rect( x, y, width, height, EDIT_HANDLE_COLOR, EDIT_HANDLE_ALPHA );
				_target.DrawEventLines( x + halfWidth, y + halfHeight );
			}
			
			super.render();
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			showHandle = true;
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			showHandle = false;
		}
		
		/**
		 * Return the key as an XML object for saving
		 * 
		 * @return
		 * Serialized elevator object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <pressureplate x={x} y={y} width={width} height={height} />;
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load pressure plate from an XML object
		 * 
		 * @param	node
		 * XML object containing pressure plate definition
		 * @return
		 * Newly created pressure plate object
		 */
		public static function Deserialize( node:XML ):PressurePlate
		{
			var pressurePlate:PressurePlate = new PressurePlate( new Point( node.@x, node.@y ), node.@width, node.@height );
			pressurePlate._target = new EventTargeter( node );
			if ( node.@name != null )
				pressurePlate.name = node.@name;
			
			return pressurePlate;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function get activated():Boolean
		{
			return _activated;
		}
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function set activated( value:Boolean ):void
		{
			_activated = value;
			_target.FireEvent( TRIGGER_EVENTS[PRESS_EVENT], value ? 1 : 0 );
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}
	}

}