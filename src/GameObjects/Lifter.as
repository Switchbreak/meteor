package GameObjects
{
	import flash.display.BlendMode;
	import Interfaces.*;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.Sfx;
	import net.flashpunk.tweens.sound.SfxFader;
	import net.flashpunk.utils.Draw;
	import net.flashpunk.utils.Input;
	import net.flashpunk.FP;
	
	/**
	 * Game object to lift and throw player
	 * 
	 * @author Switchbreak
	 */
	public class Lifter extends Entity 
		implements IEditHandle, IActivatable, ITriggerable, ISavable
	{
		// Embeds
		[Embed(source = "../../snd/lifter_wind.mp3")] private static const LIFTER_SOUND:Class;
		
		// Constants
		private static const EDIT_HANDLE_COLOR:uint = 0xff0000;
		private static const EDIT_HANDLE_ALPHA:Number = 0.5;
		private static const LIFT_POWER:Number = 500;
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Toggle"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const PARTICLE_COUNT:int = 100;
		private static const PARTICLE_SPEED:Number = 10;
		private static const PARTICLE_SPEED_RANGE:Number = 5;
		private static const PARTICLE_COLOR:uint = 0x00ffff;
		private static const PARTICLE_ALPHA:Number = 0.5;
		private static const PARTICLE_SIZE:int = 1;
		private static const PARTICLE_SIZE_RANGE:int = 2;
		private static const LIFTER_VOLUME:Number = 0.2;
		
		// Private variables
		private var placing:int = 0;
		private var _activated:Boolean = false;
		private var lifting:Boolean;
		private var showHandle:Boolean = false;
		private var particleSpeeds:Vector.<Number>;
		
		// Graphics
		private var particles:Vector.<Image>;
		
		// Objects
		private var lifterSound:Sfx;
		private var lifterSoundFader:SfxFader;
		
		/**
		 * Create and initialize the entity
		 * 
		 * @param	editMode
		 * Set to true to start object in edit mode, false to create pre-placed
		 * object
		 * @param	setX1
		 * Position of the top-left corner
		 * @param	setY1
		 * Position of the top-left corner
		 * @param	setX2
		 * Position of the bottom-right corner
		 * @param	setY2
		 * Position of the bottom-right corner
		 */
		public function Lifter( editMode:Boolean = true, setX:int = 0, setY:int = 0, setWidth:int = 0, setHeight:int = 0, setLifting:Boolean = true )
		{
			// Set object properties
			x = setX;
			y = setY;
			setHitbox( setWidth, setHeight );
			lifting = setLifting;
			type = GameWorld.ACTIVATABLE_TYPE;
			
			lifterSound = new Sfx( LIFTER_SOUND );
			//lifterSoundFader = new SfxFader(lifterSound);
			
			// Initialize particles
			particles = new Vector.<Image>( PARTICLE_COUNT );
			particleSpeeds = new Vector.<Number>( PARTICLE_COUNT );
			for ( var i:int = 0; i < PARTICLE_COUNT; ++i )
			{
				particleSpeeds[i] = PARTICLE_SPEED + FP.random * (PARTICLE_SPEED_RANGE << 1) - PARTICLE_SPEED_RANGE;
				
				var particleSize:int = PARTICLE_SIZE + FP.rand( PARTICLE_SIZE_RANGE );
				particles[i] = Image.createRect( particleSize, particleSize, PARTICLE_COLOR, PARTICLE_ALPHA );
				particles[i].x = FP.rand( setWidth - particleSize );
				particles[i].y = FP.rand( setHeight - particleSize );
				
				addGraphic( particles[i] );
			}
			
			// If no bounds are set, go into edit mode
			if ( editMode )
			{
				placing = 1;
				showHandle = true;
				graphic.visible = false;
			}
			else
			{
				graphic.visible = lifting;
			}
			
			if ( lifting )
				lifterSound.play( 0 );
		}
		
		/**
		 * Handle input for edit mode
		 */
		override public function update():void 
		{
			// Handle input for edit mode
			if ( placing > 0 )
			{
				// Size the handle box when placing the second point
				if ( placing == 2 )
				{
					setHitbox( world.mouseX - x, world.mouseY - y );
				}
				
				// Set corner on mouse down
				if ( Input.mousePressed )
				{
					// Set whichever corner we are currently placing
					if ( placing == 1 )
					{
						x = world.mouseX;
						y = world.mouseY;
						placing = 2;
					}
					else
					{
						if ( width < 0 )
						{
							x += width;
							width = -width;
						}
						if ( height < 0 )
						{
							y += height;
							height = -height;
						}
						
						placing = 0;
					}
				}
			}
			
			// Run particle effect if lifter is active
			if ( lifting )
			{
				for ( var i:int = 0; i < PARTICLE_COUNT; ++i )
				{
					particles[i].y -= particleSpeeds[i];
					if ( particles[i].y < 0 )
					{
						particles[i].y = height - particles[i].height;
					}
				}
				
				/*if ( onCamera )
					if(lifterSound.volume == 0) lifterSound.volume = LIFTER_VOLUME, 0.3);
				else
					if(lifterSound.volume == LIFTER_VOLUME) lifterSoundFader.fadeTo(0, 0.3);
			    */
			    if ( onCamera )
			    {
					if(lifterSound.volume == 0) lifterSound.volume = LIFTER_VOLUME;
				}
				else
				{
					if(lifterSound.volume == LIFTER_VOLUME) lifterSound.volume = 0;
			    }
			}
			
			super.update();
		}
		
		/**
		 * Draw dynamic edit handle if needed
		 */
		override public function render():void 
		{
			if ( showHandle )
			{
				Draw.rect( x, y, width, height, EDIT_HANDLE_COLOR, EDIT_HANDLE_ALPHA );
			}
			
			super.render();
		}
		
		/**
		 * Trigger the lifter
		 */
		public function Trigger( event:String, value:int ):void
		{
			lifting = !lifting;
			
			graphic.visible = lifting;
			
			if ( lifting )
			{
				lifterSound.play( LIFTER_VOLUME );
			}
			else if ( lifterSound.playing )
			{
			    lifterSoundFader = new SfxFader(lifterSound);
				this.addTween(lifterSoundFader);
				
				lifterSoundFader.fadeTo(0, 0.5);
			}
			
			if ( _activated )
			{
				if ( lifting )
				{
					GameWorld.currentInstance.player.accelerationY = -LIFT_POWER;
				}
				else
				{
					GameWorld.currentInstance.player.accelerationY = Player.GRAVITY;
				}
			}
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			showHandle = true;
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			showHandle = false;
		}
		
		/**
		 * Return the lifter as an XML object for saving
		 * 
		 * @return
		 * Serialized door object
		 */
		public function GetSaveObject():XML
		{
			return <lifter x={x} y={y} width={width} height={height} name={name} />;
		}
		
		/**
		 * Load key from an XML object
		 * 
		 * @param	node
		 * XML object containing key definition
		 * @return
		 * Newly created key object
		 */
		public static function Deserialize( node:XML ):Lifter
		{
			var lifter:Lifter = new Lifter( false, node.@x, node.@y, node.@width, node.@height );
			if ( node.@name != null )
				lifter.name = node.@name;
			
			return lifter;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function get activated():Boolean
		{
			return _activated;
		}
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function set activated( value:Boolean ):void
		{
			_activated = value;
			
			if ( _activated && lifting )
			{
				GameWorld.currentInstance.player.accelerationY = -LIFT_POWER;
			}
			else
			{
				GameWorld.currentInstance.player.accelerationY = Player.GRAVITY;
			}
		}
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
	}

}