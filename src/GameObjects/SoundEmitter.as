package GameObjects 
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import Interfaces.*;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	
	/**
	 * Game object to provide a logical AND gate
	 * 
	 * @author Switchbreak
	 */
	public class SoundEmitter extends Entity
		implements ITriggerable, IEditHandle, ISavable
	{
		// Embeds
		[Embed(source = "../../img/Speaker.png")] private static const EDIT_HANDLE:Class;
		[Embed(source = "../../snd/KeySpawn.mp3")] private static const KEY_SPAWN:Class;
		
		// Constants
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Play"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const KEY_SPAWN_VOLUME:Number = 0.5;
		private static const EDIT_HANDLE_SIZE:int = 16;
		
		// Public variables
		private var source:String;
		private var sound:Sfx;
		private var volume:Number;
		
		/**
		 * Create and initialize the game object
		 * 
		 * @param	startPosition
		 * Position of the item spawn
		 * @param	setSpawnType
		 * Type of object to spawn when triggered
		 */
		public function SoundEmitter( startPosition:Point = null, soundName:String = "KeySpawn" ) 
		{
			super();
			
			source = soundName;
			switch( source )
			{
				case "KeySpawn":
					sound = new Sfx( KEY_SPAWN );
					volume = KEY_SPAWN_VOLUME;
					break;
			}
			
			// If placing the object for the first time, show the edit handle
			if ( startPosition == null )
			{
				ShowHandle();
				setHitboxTo( graphic );
			}
			else 
			{
				x = startPosition.x;
				y = startPosition.y;
				setHitbox( EDIT_HANDLE_SIZE, EDIT_HANDLE_SIZE );
			}
			
			// Set object values
			active = false;
		}
		
		/**
		 * Trigger the spawn
		 */
		public function Trigger( event:String, value:int ):void
		{
			sound.play( volume );
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			if ( graphic == null )
				graphic = new Stamp( EDIT_HANDLE );
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			graphic = null;
		}
		
		/**
		 * Return the sound emitter as an XML object for saving
		 * 
		 * @return
		 * Serialized sound emitter object
		 */
		public function GetSaveObject():XML
		{
			return <soundemitter x={x} y={y} source={source} name={name} />;
		}
		
		/**
		 * Load sound emitter from an XML object
		 * 
		 * @param	node
		 * XML object containing sound emitter definition
		 * @return
		 * Newly created sound emitter object
		 */
		public static function Deserialize( node:XML ):SoundEmitter
		{
			var soundEmitter:SoundEmitter = new SoundEmitter( new Point( node.@x, node.@y ), node.@source );
			if ( node.@name != null )
				soundEmitter.name = node.@name;
			
			return soundEmitter;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
	}

}