package GameObjects 
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import Interfaces.*;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	
	/**
	 * Game object for spawning other game objects when triggered
	 * 
	 * @author Switchbreak
	 */
	public class Spawn extends Entity
		implements ITriggerable, IEditHandle, ISavable
	{
		// Embeds
		[Embed(source = "../../img/cog.png")] private static const EDIT_HANDLE:Class;
		
		// Constants
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Spawn"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const EDIT_HANDLE_SIZE:int = 16;
		
		// Private variables
		private var spawnType:String;
		
		/**
		 * Create and initialize the game object
		 * 
		 * @param	startPosition
		 * Position of the item spawn
		 * @param	setSpawnType
		 * Type of object to spawn when triggered
		 */
		public function Spawn( startPosition:Point = null, setSpawnType:String = "key" ) 
		{
			super();
			
			// If placing the object for the first time, show the edit handle
			if ( startPosition == null )
			{
				ShowHandle();
				setHitboxTo( graphic );
			}
			else 
			{
				x = startPosition.x;
				y = startPosition.y;
				setHitbox( EDIT_HANDLE_SIZE, EDIT_HANDLE_SIZE );
			}
			
			// Set object values
			active = false;
			spawnType = setSpawnType;
		}
		
		/**
		 * Trigger the spawn
		 */
		public function Trigger( event:String, value:int ):void
		{
			if ( value > 0 )
			{
				switch( spawnType )
				{
					case "key":
						world.add( new KeyObject( x, y ) );
						world.remove( this );
						
						break;
				}
			}
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			if ( graphic == null )
				graphic = new Stamp( EDIT_HANDLE );
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			graphic = null;
		}
		
		/**
		 * Return the spawn as an XML object for saving
		 * 
		 * @return
		 * Serialized spawn object
		 */
		public function GetSaveObject():XML
		{
			return <spawn x={x} y={y} spawnType={spawnType} name={name} />;
		}
		
		/**
		 * Load spawn from an XML object
		 * 
		 * @param	node
		 * XML object containing spawn definition
		 * @return
		 * Newly created spawn object
		 */
		public static function Deserialize( node:XML ):Spawn
		{
			var spawn:Spawn = new Spawn( new Point( node.@x, node.@y ), node.@spawnType );
			if ( node.@name != null )
				spawn.name = node.@name;
			
			return spawn;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
	}

}