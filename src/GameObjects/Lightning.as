package GameObjects 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Spritemap;
	import Interfaces.*;
	import net.flashpunk.tweens.misc.Alarm;
	
	/**
	 * Image that switches frames on being triggered
	 * 
	 * @author Switchbreak
	 */
	public class Lightning extends Entity
		implements ITriggerable, ISavable, IEventFirable, IEditHandle
	{
		// Embeds
		[Embed(source = "../../img/Lightning.png")] private static const LIGHTNING:Class;
		
		// Constants
		private static const WIDTH:int = 31;
		private static const HEIGHT:int = 87;
		private static const HITBOX_WIDTH:int = 8;
		private static const BOLT_ANIM:String = "Bolt";
		private static const FIRE_DURATION:Number = 0.5;
		private static const END_EVENT:int = 0;
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Fire", "Disable"] );
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["End"] );
		private static const PROPERTIES:Vector.<String> = null;
		
		// Private variables
		private var firing:Boolean = false;
		private var enabled:Boolean = true;
		private var editHandle:Boolean = false;
		
		// Public variables
		public var _target:EventTargeter;
		
		// Graphics
		private var lightningSprite:Spritemap;
		
		/**
		 * Create and initialize the game object
		 */
		public function Lightning( setX:int = 0, setY:int = 0 ) 
		{
			// Set object values
			lightningSprite = new Spritemap( LIGHTNING, WIDTH, HEIGHT );
			lightningSprite.add( BOLT_ANIM, [1, 2, 3], 20, true );
			_target = new EventTargeter();
			
			super( setX, setY, lightningSprite );
			setHitbox( HITBOX_WIDTH, HEIGHT, -((WIDTH >> 1) - (HITBOX_WIDTH >> 1)), 0 );
			
			active = false;
			lightningSprite.frame = 0;
		}
		
		/**
		 * Handle collision with player
		 */
		override public function update():void 
		{
			if ( collide( Player.PLAYER_TYPE, x, y ) != null )
			{
				GameWorld.TriggerEvent( "GameOver" );
			}
			
			super.update();
		}
		
		/**
		 * Draw event lines if edit handles are showing
		 */
		override public function render():void 
		{
			if ( editHandle )
				_target.DrawEventLines( x + halfWidth, y + halfHeight );
			
			super.render();
		}
		
		/**
		 * Trigger the lightning bolt
		 */
		public function Trigger( event:String, value:int ):void
		{
			switch( event )
			{
				case "Fire":
					if ( enabled )
					{
						active = true;
						firing = true;
						lightningSprite.play( BOLT_ANIM );
						addTween( new Alarm( FIRE_DURATION, DoneFiring, ONESHOT ), true );
					}
					
					break;
				case "Disable":
					firing = false;
					active = false;
					lightningSprite.frame = 0;
					enabled = !enabled;
					
					break;
			}
		}
		
		/**
		 * Called when lightning bolt is done firing
		 */
		private function DoneFiring():void
		{
			firing = false;
			active = false;
			lightningSprite.frame = 0;
			
			if ( _target )
			{
				_target.FireEvent( TRIGGER_EVENTS[END_EVENT] );
			}
		}
		
		/**
		 * Return the lightning as an XML object for saving
		 * 
		 * @return
		 * Serialized lightning object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <lightning x={x} y={y} name={name} />;
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load lightning from an XML object
		 * 
		 * @param	node
		 * XML object containing lightning definition
		 * @return
		 * Newly created lightning object
		 */
		public static function Deserialize( node:XML ):Lightning
		{
			var lightning:Lightning = new Lightning( node.@x, node.@y );
			lightning._target = new EventTargeter( node );
			if ( node.@name != null )
				lightning.name = node.@name;
			
			return lightning;
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			editHandle = true;
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			editHandle = false;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}		
	}

}