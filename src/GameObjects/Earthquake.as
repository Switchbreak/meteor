package GameObjects 
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import Interfaces.*;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.Sfx;
	import net.flashpunk.FP;
	
	/**
	 * Game object to provide a logical AND gate
	 * 
	 * @author Switchbreak
	 */
	public class Earthquake extends Entity
		implements ITriggerable, IEditHandle, ISavable
	{
		// Embeds
		[Embed(source="../../img/Bolt.png")] private static const EDIT_HANDLE:Class;
		
		// Constants
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Shake"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const DURATION:Number = 1;
		private static const AMOUNT:Number = 2;
		private static const EDIT_HANDLE_SIZE:int = 16;
		
		// Private variables
		private var duration:Number = 0;
		
		/**
		 * Create and initialize the game object
		 * 
		 * @param	startPosition
		 * Position of the item spawn
		 * @param	setSpawnType
		 * Type of object to spawn when triggered
		 */
		public function Earthquake( startPosition:Point = null ) 
		{
			super();
			
			// If placing the object for the first time, show the edit handle
			if ( startPosition == null )
			{
				ShowHandle();
				setHitboxTo( graphic );
			}
			else 
			{
				x = startPosition.x;
				y = startPosition.y;
				setHitbox( EDIT_HANDLE_SIZE, EDIT_HANDLE_SIZE );
			}
			
			// Set object values
			active = false;
		}
		
		override public function update():void 
		{
			duration -= FP.elapsed;
			if ( duration <= 0 )
			{
				FP.screen.x = FP.screen.y = 0;
				active = false;
			}
			else
			{
				FP.screen.x = FP.random * (AMOUNT << 1) - AMOUNT;
				FP.screen.y = FP.random * (AMOUNT << 1) - AMOUNT;
			}
			
			super.update();
		}
		
		/**
		 * Trigger the spawn
		 */
		public function Trigger( event:String, value:int ):void
		{
			active = true;
			duration = DURATION;
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			if ( graphic == null )
				graphic = new Stamp( EDIT_HANDLE );
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			graphic = null;
		}
		
		/**
		 * Return the earthquake as an XML object for saving
		 * 
		 * @return
		 * Serialized earthquake object
		 */
		public function GetSaveObject():XML
		{
			return <earthquake x={x} y={y} name={name} />;
		}
		
		/**
		 * Load earthquake from an XML object
		 * 
		 * @param	node
		 * XML object containing earthquake definition
		 * @return
		 * Newly created earthquake object
		 */
		public static function Deserialize( node:XML ):Earthquake
		{
			var earthQuake:Earthquake = new Earthquake( new Point( node.@x, node.@y ) );
			if ( node.@name != null )
				earthQuake.name = node.@name;
			
			return earthQuake;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
	}

}