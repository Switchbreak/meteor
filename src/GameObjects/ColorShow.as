package GameObjects 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import Interfaces.*;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.FP;
	
	/**
	 * Image that switches frames on being triggered
	 * 
	 * @author Switchbreak
	 */
	public class ColorShow extends Entity implements ITriggerable, ISavable 
	{
		// Constants
		private static const RADIUS:int = 10;
		private static const CYCLE_DURATION:Number = 0.5;
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Show"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const COLORS:Vector.<uint> = Vector.<uint>( [ 0xff0000, 0x00ffff, 0xffff00 ] );;
		
		// Private variables
		private var cycling:Boolean = false;
		private var cycleTimer:Alarm;
		private var colorIndex:int = 0;
		
		// Graphics
		private var circleSprite:Image;
		
		/**
		 * Create and initialize the game object
		 */
		public function ColorShow( setX:int = 0, setY:int = 0 ) 
		{
			circleSprite = Image.createCircle( RADIUS );
			circleSprite.color = COLORS[colorIndex];
			
			if ( setX != 0 || setY != 0 )
				circleSprite.visible = false;
			
			super( setX, setY, circleSprite );
			setHitboxTo( graphic );
			
			cycleTimer = new Alarm( CYCLE_DURATION, CycleColor, LOOPING );
			addTween( cycleTimer, false );
		}
		
		/**
		 * Trigger the color cycler
		 */
		public function Trigger( event:String, value:int ):void
		{
			if ( !cycling )
			{
				colorIndex = 0;
				circleSprite.color = COLORS[colorIndex];
				circleSprite.visible = true;
				
				cycling = true;
				cycleTimer.start();
			}
		}
		
		/**
		 * Called when the color cycles
		 */
		private function CycleColor():void
		{
			if ( ++colorIndex >= COLORS.length )
			{
				cycling = false;
				cycleTimer.active = false;
				circleSprite.visible = false;
			}
			else
			{
				circleSprite.color = COLORS[colorIndex];
			}
		}
		
		/**
		 * Return the color cycler as an XML object for saving
		 * 
		 * @return
		 * Serialized color cycler object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <colorshow x={x} y={y} name={name} />;
			
			return node;
		}
		
		/**
		 * Load color cycler from an XML object
		 * 
		 * @param	node
		 * XML object containing color cycler definition
		 * @return
		 * Newly created color cycler object
		 */
		public static function Deserialize( node:XML ):ColorShow
		{
			var colorShow:ColorShow = new ColorShow( node.@x, node.@y );
			if ( node.@name != null )
				colorShow.name = node.@name;
			
			return colorShow;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
		
	}

}