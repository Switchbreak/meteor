package GameObjects 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Spritemap;
	import Interfaces.*;
	
	/**
	 * Image that switches frames on being triggered
	 * 
	 * @author Switchbreak
	 */
	public class ImageSwitcher extends Entity implements ITriggerable, ISavable 
	{
		// Embeds
		[Embed(source = "../../img/LeftBranchVines.png")] private static const LEFT_BRANCH_VINES:Class;
		[Embed(source = "../../img/RightBranchVines.png")] private static const RIGHT_BRANCH_VINES:Class;
		[Embed(source = "../../img/TreeVineLeft.png")] private static const TREE_VINE_LEFT:Class;
		[Embed(source = "../../img/TreeVineRight.png")] private static const TREE_VINE_RIGHT:Class;
		[Embed(source = "../../img/Plant.png")] private static const PLANT:Class;
		
		// Constants
		private static const LEFT_BRANCH_VINES_WIDTH:int = 491;
		private static const LEFT_BRANCH_VINES_HEIGHT:int = 71;
		private static const RIGHT_BRANCH_VINES_WIDTH:int = 491;
		private static const RIGHT_BRANCH_VINES_HEIGHT:int = 77;
		private static const TREE_VINE_LEFT_WIDTH:int = 364;
		private static const TREE_VINE_LEFT_HEIGHT:int = 77;
		private static const TREE_VINE_RIGHT_WIDTH:int = 251;
		private static const TREE_VINE_RIGHT_HEIGHT:int = 59;
		private static const PLANT_WIDTH:int = 119;
		private static const PLANT_HEIGHT:int = 73;
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Switch"] );
		private static const PROPERTIES:Vector.<String> = null;
		
		// Graphics
		private var source:String;
		private var switchSprite:Spritemap;
		
		/**
		 * Create and initialize the game object
		 */
		public function ImageSwitcher( switcherImage:String = "LeftBranch", setX:int = 0, setY:int = 0 ) 
		{
			// Set object values
			active = false;
			source = switcherImage;
			
			// Choose graphic
			switch( switcherImage )
			{
				case "LeftBranch":
					switchSprite = new Spritemap( LEFT_BRANCH_VINES, LEFT_BRANCH_VINES_WIDTH, LEFT_BRANCH_VINES_HEIGHT );
					break;
				case "RightBranch":
					switchSprite = new Spritemap( RIGHT_BRANCH_VINES, RIGHT_BRANCH_VINES_WIDTH, RIGHT_BRANCH_VINES_HEIGHT );
					break;
				case "TreeLeft":
					switchSprite = new Spritemap( TREE_VINE_LEFT, TREE_VINE_LEFT_WIDTH, TREE_VINE_LEFT_HEIGHT );
					break;
				case "TreeRight":
					switchSprite = new Spritemap( TREE_VINE_RIGHT, TREE_VINE_RIGHT_WIDTH, TREE_VINE_RIGHT_HEIGHT );
					break;
				case "Plant":
					switchSprite = new Spritemap( PLANT, PLANT_WIDTH, PLANT_HEIGHT );
					break;
			}
			
			super( setX, setY, switchSprite );
			setHitboxTo( graphic );
		}
		
		/**
		 * Trigger the switcher
		 */
		public function Trigger( event:String, value:int ):void
		{
			switchSprite.frame = value;
		}
		
		/**
		 * Return the switcher as an XML object for saving
		 * 
		 * @return
		 * Serialized switcher object
		 */
		public function GetSaveObject():XML
		{
			return <imageswitcher source={source} x={x} y={y} name={name} />;
		}
		
		/**
		 * Load switcher from an XML object
		 * 
		 * @param	node
		 * XML object containing switcher definition
		 * @return
		 * Newly created switcher object
		 */
		public static function Deserialize( node:XML ):ImageSwitcher
		{
			var switcher:ImageSwitcher = new ImageSwitcher( node.@source, node.@x, node.@y );
			if ( node.@name != null )
				switcher.name = node.@name;
			
			return switcher;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
		
	}

}