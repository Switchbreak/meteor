package GameObjects
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.Sfx;
	import net.flashpunk.tweens.motion.LinearMotion;
	import net.flashpunk.utils.Ease;
	import net.flashpunk.utils.Draw;
	import Interfaces.*;
	
	/**
	 * Door game object
	 * 
	 * @author Switchbreak
	 */
	public class Door extends Entity
		implements IActivatable, ISavable, ITriggerable, IEventFirable, IEditHandle
	{
		// Embeds
		[Embed(source="../../snd/door.mp3")] private static const DOOR_SND:Class;
		
		// Constants
		private static const DOOR_WIDTH:int = 10;
		private static const OPEN_DURATION:Number = 2.0;
		private static const DOOR_VOLUME:Number = 0.3;
		private static const DOOR_OPEN_EVENT:int = 0;
		private static const DOOR_CLOSE_EVENT:int = 1;
		private static const DOOR_COLOR:uint = 0x008800;
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Open", "Close"] );
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["open", "close"] );
		private static const PROPERTIES:Vector.<String> = Vector.<String>( ["locked"] );
		
		// Private variables
		private var collisionLine1:Line = null;
		private var collisionLine2:Line = null;
		private var _activated:Boolean = false;
		private var doorOpened:Boolean = false;
		private var closePoint:Point;
		private var editHandle:Boolean = false;
		
		// Public variables
		public var _target:EventTargeter;
		public var locked:Boolean = true;
		
		// Objects
		private var openSfx:Sfx = new Sfx( DOOR_SND );
		private var drawLines:DrawLines = null;
		private var openTween:LinearMotion;
		
		/**
		 * Create and initialize the door
		 */
		public function Door( end1:Point = null, end2:Point = null )
		{
			// Set object values
			active = false;
			type = GameWorld.ACTIVATABLE_TYPE;
			_target = new EventTargeter();
			
			// If endpoints were passed in, create the line - otherwise, create
			// the line in level edit mode
			if ( end1 != null && end2 != null )
			{
				// Create collision line
				SetLine( new Line( end1, end2 ) );
			}
		}
		
		/**
		 * Add child objects when added to the world
		 */
		override public function added():void 
		{
			// Add collision lines
			if ( collisionLine1 != null && collisionLine2 != null )
			{
				(world as GameWorld).collisionLines.push( collisionLine1 );
				(world as GameWorld).collisionLines.push( collisionLine2 );
			}
			else
			{
				drawLines = world.add( new DrawLines( SetLine ) ) as DrawLines;
			}
		}
		
		/**
		 * Cleanup when object is removed
		 */
		override public function removed():void 
		{
			if ( collisionLine1 != null )
				(world as GameWorld).collisionLines.splice( (world as GameWorld).collisionLines.indexOf( collisionLine1 ), 1 );
			if ( collisionLine2 != null )
				(world as GameWorld).collisionLines.splice( (world as GameWorld).collisionLines.indexOf( collisionLine2 ), 1 );
			if ( drawLines != null )
				world.remove( drawLines );
			
			super.removed();
		}
		
		/**
		 * Show the edit handle if enabled
		 */
		override public function render():void 
		{
			if ( editHandle )
				_target.DrawEventLines( x + halfWidth, y + halfHeight );
			
			if( collisionLine1 != null )
				Draw.line( collisionLine1.end1.x, collisionLine1.end1.y, collisionLine1.end2.x, collisionLine1.end2.y, DOOR_COLOR );
			
			super.render();
		}
		
		/**
		 * Sets the collision line for the door
		 * 
		 * @param	setLine
		 * Collision line 
		 */
		public function SetLine( setLine:Line ):void
		{
			// Make double-sided line
			collisionLine1 = setLine;
			collisionLine1.parent = this;
			collisionLine2 = new Line( setLine.end2, setLine.end1 );
			collisionLine2.parent = this;
			
			// Add child lines if object world has been set
			if ( world )
			{
				GameWorld.currentInstance.collisionLines.push( collisionLine1 );
				GameWorld.currentInstance.collisionLines.push( collisionLine2 );
			}
			
			// Set collision box for door activation
			closePoint = setLine.end2.clone();
			x = setLine.end1.x;
			y = setLine.end1.y;
			setHitbox( DOOR_WIDTH, Math.abs( setLine.end2.y - setLine.end1.y ), (DOOR_WIDTH >> 1), 0 );
		}
		
		/**
		 * Return the key as an XML object for saving
		 * 
		 * @return
		 * Serialized door object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <door x1={collisionLine1.end1.x} y1={collisionLine1.end1.y} x2={collisionLine1.end2.x} y2={collisionLine1.end2.y} name={name} />;
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load door from an XML object
		 * 
		 * @param	buttonXML
		 * XML object containing button definition
		 * @return
		 * Newly created button object
		 */
		public static function Deserialize( node:XML ):Door
		{
			var door:Door = new Door( new Point( node.@x1, node.@y1 ), new Point( node.@x2, node.@y2 ) )
			
			door._target = new EventTargeter( node );
			if ( node.@name != null )
				door.name = node.@name;
			
			return door;
		}
		
		/**
		 * Handle motion
		 */
		override public function update():void 
		{
			// Follow door opening animation
			collisionLine1.end2.x = openTween.x;
			collisionLine1.end2.y = openTween.y;
			//collisionLine2.end1.x = openTween.x;
			//collisionLine2.end1.y = openTween.y;
			
			super.update();
		}
		
		/**
		 * Opens the door
		 */
		private function OpenDoor():void
		{
			if ( doorOpened )
				return;
			
			openSfx.play( DOOR_VOLUME );
			
			if ( openTween )
			{
				openTween.cancel();
			}
			
			openTween = new LinearMotion( OpenFinished, ONESHOT );
			openTween.setMotion( collisionLine1.end2.x, collisionLine1.end2.y, collisionLine1.end1.x, collisionLine1.end1.y, OPEN_DURATION, Ease.quadOut );
			addTween( openTween, true );
			
			doorOpened = true;
			active = true;
			
			// Trigger door open event if set
			if ( _target != null )
				_target.FireEvent( TRIGGER_EVENTS[DOOR_OPEN_EVENT] );
		}
		
		/**
		 * Closes the door
		 */
		private function CloseDoor():void
		{
			if ( !doorOpened )
				return;
			
			openSfx.play( DOOR_VOLUME );
			
			if ( openTween )
			{
				openTween.cancel();
			}
			
			openTween = new LinearMotion( CloseFinished, ONESHOT );
			openTween.setMotion( collisionLine1.end2.x, collisionLine1.end2.y, closePoint.x, closePoint.y, OPEN_DURATION, Ease.quadOut );
			addTween( openTween, true );
			
			doorOpened = false;
			active = true;
		}
		
		/**
		 * Door has finished opening
		 */
		private function OpenFinished():void
		{
			active = false;
		}
		
		/**
		 * Door has finished closing
		 */
		private function CloseFinished():void
		{
			active = false;
			
			// Trigger door open event if set
			if ( _target != null )
				_target.FireEvent( TRIGGER_EVENTS[DOOR_CLOSE_EVENT] );
		}
		
		/**
		 * Trigger the object
		 */
		public function Trigger( event:String, value:int ):void
		{
			switch( event )
			{
				case "Open":
					OpenDoor();
					break;
				case "Close":
					CloseDoor();
					break;
			}
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			editHandle = true;
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			editHandle = false;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function get activated():Boolean
		{
			return _activated;
		}
		
		/**
		 * Gets or sets the activated state of the object
		 */
		public function set activated( value:Boolean ):void
		{
			// If value is true, player has collided with the key and 
			if ( value == true && doorOpened == false )
			{
				if ( locked )
				{
					// Check if the player has a key
					for each( var item:Entity in GameWorld.currentInstance.inventory.objects )
					{
						if ( item is KeyObject )
						{
							locked = false;
							GameWorld.currentInstance.inventory.RemoveObject( item );
							break;
						}
					}
				}
				
				if ( !locked )
				{
					OpenDoor();
				}
			}
			_activated = value;
		}
		
		/**
		 * Get list of input trigger events for editor
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}		
	}

}