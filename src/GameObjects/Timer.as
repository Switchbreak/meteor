package GameObjects 
{
	import flash.geom.Point;
	import Interfaces.*;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.tweens.misc.Alarm;
	
	/**
	 * Timer game object
	 * 
	 * @author Switchbreak
	 */
	public class Timer extends Entity
		implements IEditHandle, ISavable, IEventFirable
	{
		// Embeds
		[Embed(source = "../../img/clock.png")] private static const EDIT_HANDLE:Class;
		
		// Constants
		private static const TIMER_EVENT:int = 0;
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["alarm"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const EDIT_HANDLE_SIZE:int = 16;
		
		// Private variables
		private var duration:Number;
		private var looping:Boolean;
		private var editHandle:Boolean = false;
		
		// Public variables
		public var _target:EventTargeter;
		
		/**
		 * Create and initialize the timer
		 */
		public function Timer( startPosition:Point = null, setDuration:Number = 2.0, setLooping:Boolean = true )
		{
			super();
			
			// If placing the object for the first time, show the edit handle
			if ( startPosition == null )
			{
				ShowHandle();
				setHitboxTo( graphic );
			}
			else
			{
				x = startPosition.x;
				y = startPosition.y;
				setHitbox( EDIT_HANDLE_SIZE, EDIT_HANDLE_SIZE );
			}
			
			// Set object values
			duration = setDuration;
			looping = setLooping;
			_target = new EventTargeter();
			
			// Start timer
			addTween( new Alarm( duration, Fire, looping ? LOOPING : ONESHOT ), true );
		}
		
		/**
		 * Draw event lines if edit handles are showing
		 */
		override public function render():void 
		{
			if ( editHandle )
				_target.DrawEventLines( x + halfWidth, y + halfHeight );
			
			super.render();
		}
		
		/**
		 * Trigger the timer target
		 */
		private function Fire():void
		{
			if( _target )
				_target.FireEvent( TRIGGER_EVENTS[TIMER_EVENT] );
			
			if ( !looping )
				world.remove( this );
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			graphic = new Stamp( EDIT_HANDLE );
			editHandle = true;
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			graphic = null;
			editHandle = false;
		}
		
		/**
		 * Return the key as an XML object for saving
		 * 
		 * @return
		 * Serialized key object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <timer x={x} y={y} duration={duration} looping={looping} name={name} />;
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load timer from an XML object
		 * 
		 * @param	node
		 * XML object containing timer definition
		 * @return
		 * Newly created timer object
		 */
		public static function Deserialize( node:XML ):Timer
		{
			var timer:Timer = new Timer( new Point( node.@x, node.@y ), node.@duration, node.@looping );
			timer._target = new EventTargeter( node );
			if ( node.@name != null )
				timer.name = node.@name;
			
			return timer;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}
	}

}