package GameObjects.Logic 
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import Interfaces.*;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	
	/**
	 * Game object to provide a logical AND gate
	 * 
	 * @author Switchbreak
	 */
	public class OnceGate extends Entity
		implements ITriggerable, IEditHandle, ISavable, IEventFirable
	{
		// Embeds
		[Embed(source="../../../img/cog.png")] private static const EDIT_HANDLE:Class;
		
		// Constants
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Fire"] );
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["fire"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const FIRE_EVENT:int = 0;
		private static const EDIT_HANDLE_SIZE:int = 16;
		
		// Public variables
		public var _target:EventTargeter;
		
		// Private variables
		private var editHandle:Boolean = false;
		
		/**
		 * Create and initialize the game object
		 * 
		 * @param	startPosition
		 * Position of the item spawn
		 * @param	setSpawnType
		 * Type of object to spawn when triggered
		 */
		public function OnceGate( startPosition:Point = null ) 
		{
			super();
			
			// If placing the object for the first time, show the edit handle
			if ( startPosition == null )
			{
				ShowHandle();
				setHitboxTo( graphic );
			}
			else 
			{
				x = startPosition.x;
				y = startPosition.y;
				setHitbox( EDIT_HANDLE_SIZE, EDIT_HANDLE_SIZE );
			}
			
			// Set object values
			active = false;
			_target = new EventTargeter();
		}
		
		/**
		 * Draw event lines if edit handles are showing
		 */
		override public function render():void 
		{
			if ( editHandle )
				_target.DrawEventLines( x + halfWidth, y + halfHeight );
			
			super.render();
		}
		
		/**
		 * Trigger the spawn
		 */
		public function Trigger( event:String, value:int ):void
		{
			_target.FireEvent( TRIGGER_EVENTS[FIRE_EVENT], 1 );
			world.remove( this );
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			editHandle = true;
			if ( graphic == null )
				graphic = new Stamp( EDIT_HANDLE );
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			editHandle = false;
			graphic = null;
		}
		
		/**
		 * Return the once gate as an XML object for saving
		 * 
		 * @return
		 * Serialized once gate object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <oncegate x={x} y={y} name={name} />;
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load once gate from an XML object
		 * 
		 * @param	node
		 * XML object containing once gate definition
		 * @return
		 * Newly created once gate object
		 */
		public static function Deserialize( node:XML ):OnceGate
		{
			var onceGate:OnceGate = new OnceGate( new Point( node.@x, node.@y ) );
			onceGate._target = new EventTargeter( node );
			if ( node.@name != null )
				onceGate.name = node.@name;
			
			return onceGate;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}
	}

}