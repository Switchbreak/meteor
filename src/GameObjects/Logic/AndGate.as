package GameObjects.Logic 
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import Interfaces.*;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	
	/**
	 * Game object to provide a logical AND gate
	 * 
	 * @author Switchbreak
	 */
	public class AndGate extends Entity
		implements ITriggerable, IEditHandle, ISavable, IEventFirable
	{
		// Embeds
		[Embed(source = "../../../img/ampersand.png")] private static const EDIT_HANDLE:Class;
		
		// Constants
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Param1", "Param2"] );
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["fire"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const FIRE_EVENT:int = 0;
		private static const EDIT_HANDLE_SIZE:int = 16;
		
		// Private variables
		private var param1:Boolean = false;
		private var param2:Boolean = false;
		private var editHandle:Boolean = false;
		
		// Public variables
		public var _target:EventTargeter;
		
		/**
		 * Create and initialize the game object
		 * 
		 * @param	startPosition
		 * Position of the item spawn
		 * @param	setSpawnType
		 * Type of object to spawn when triggered
		 */
		public function AndGate( startPosition:Point = null ) 
		{
			super();
			
			// If placing the object for the first time, show the edit handle
			if ( startPosition == null )
			{
				ShowHandle();
				setHitboxTo( graphic );
			}
			else 
			{
				x = startPosition.x;
				y = startPosition.y;
				setHitbox( EDIT_HANDLE_SIZE, EDIT_HANDLE_SIZE );
			}
			
			// Set object values
			active = false;
			_target = new EventTargeter();
		}
		
		/**
		 * Draw event lines if edit handles are showing
		 */
		override public function render():void 
		{
			if ( editHandle )
				_target.DrawEventLines( x + halfWidth, y + halfHeight );
			
			super.render();
		}
		
		/**
		 * Trigger the spawn
		 */
		public function Trigger( event:String, value:int ):void
		{
			var tempParam1:Boolean = param1;
			var tempParam2:Boolean = param2;
			
			switch( event )
			{
				case "Param1-Save":
					param1 = (value > 0);
					tempParam1 = param1;
					break;
				case "Param2-Save":
					param2 = (value > 0);
					tempParam2 = param2;
					break;
				case "Param1":
					tempParam1 = (value > 0);
					break;
				case "Param2":
					tempParam2 = (value > 0);
					break;
			}
			
			// Trigger if both inputs have been set
			if ( tempParam1 && tempParam2 )
				_target.FireEvent( TRIGGER_EVENTS[FIRE_EVENT], 1 );
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			editHandle = true;
			if ( graphic == null )
				graphic = new Stamp( EDIT_HANDLE );
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			editHandle = false;
			graphic = null;
		}
		
		/**
		 * Return the and gate as an XML object for saving
		 * 
		 * @return
		 * Serialized and gate object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <andgate x={x} y={y} name={name} />;
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load andgate from an XML object
		 * 
		 * @param	node
		 * XML object containing andgate definition
		 * @return
		 * Newly created andgate object
		 */
		public static function Deserialize( node:XML ):AndGate
		{
			var andGate:AndGate = new AndGate( new Point( node.@x, node.@y ) );
			andGate._target = new EventTargeter( node );
			if ( node.@name != null )
				andGate.name = node.@name;
			
			return andGate;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}
	}

}