package GameObjects 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Image;
	import Interfaces.*;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.FP;
	
	/**
	 * Image that switches frames on being triggered
	 * 
	 * @author Switchbreak
	 */
	public class ColorCycle extends Entity
		implements ITriggerable, ISavable, IEventFirable, IEditHandle
	{
		// Constants
		private static const RADIUS:int = 10;
		private static const CYCLE_DURATION:Number = 0.5;
		private static const CORRECT_EVENT:int = 0;
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["Stop"] );
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["Correct"] );
		private static const PROPERTIES:Vector.<String> = null;
		private static const COLORS:Vector.<uint> = Vector.<uint>( [ 0xff0000, 0x00ff00, 0x0000ff, 0xffff00, 0x00ffff, 0xff00ff ] );;
		
		// Private variables
		private var cycling:Boolean = true;
		private var cycleTimer:Alarm;
		private var colorIndex:int;
		private var correctColor:int;
		private var editHandle:Boolean = false;
		
		// Public variables
		public var _target:EventTargeter;
		
		// Graphics
		private var circleSprite:Image;
		
		/**
		 * Create and initialize the game object
		 */
		public function ColorCycle( setX:int = 0, setY:int = 0, setCorrectColor:int = -1 ) 
		{
			// Set object values
			correctColor = setCorrectColor;
			colorIndex = FP.rand( COLORS.length );
			_target = new EventTargeter();
			
			circleSprite = Image.createCircle( RADIUS );
			circleSprite.color = COLORS[colorIndex];
			
			super( setX, setY, circleSprite );
			setHitboxTo( graphic );
			
			cycleTimer = new Alarm( CYCLE_DURATION, CycleColor, LOOPING );
			addTween( cycleTimer, true );
		}
		
		/**
		 * Draw event lines if edit handles are showing
		 */
		override public function render():void 
		{
			if ( editHandle )
				_target.DrawEventLines( x + halfWidth, y + halfHeight );
			
			super.render();
		}
		
		/**
		 * Trigger the color cycler
		 */
		public function Trigger( event:String, value:int ):void
		{
			cycling = !cycling;
			cycleTimer.active = cycling;
			
			if ( _target != null )
			{
				if ( cycling )
				{
					_target.FireEvent( TRIGGER_EVENTS[CORRECT_EVENT], 0 );
				}
				else if ( colorIndex == correctColor )
				{
					_target.FireEvent( TRIGGER_EVENTS[CORRECT_EVENT], 1 );
				}
			}
		}
		
		/**
		 * Called when the color cycles
		 */
		private function CycleColor():void
		{
			colorIndex = (colorIndex + 1) % COLORS.length;
			circleSprite.color = COLORS[colorIndex];
		}
		
		/**
		 * Return the color cycler as an XML object for saving
		 * 
		 * @return
		 * Serialized color cycler object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <colorcycler x={x} y={y} name={name} correct={correctColor} />;
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load color cycler from an XML object
		 * 
		 * @param	node
		 * XML object containing color cycler definition
		 * @return
		 * Newly created color cycler object
		 */
		public static function Deserialize( node:XML ):ColorCycle
		{
			var colorCycle:ColorCycle = new ColorCycle( node.@x, node.@y, node.@correct );
			colorCycle._target = new EventTargeter( node );
			if ( node.@name != null )
				colorCycle.name = node.@name;
			
			return colorCycle;
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			editHandle = true;
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			editHandle = false;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		//
		// Properties
		//
		
		/**
		 * Gets a list of available events to trigger on the object
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}
	}

}