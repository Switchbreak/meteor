package GameObjects 
{
	import flash.geom.Point;
	import net.flashpunk.Entity;
	import net.flashpunk.utils.Draw;
	import net.flashpunk.utils.Input;
	import net.flashpunk.utils.Key;
	import Interfaces.*;
	
	/**
	 * A movement path game object
	 * 
	 * @author Switchbreak
	 */
	public class Path extends Entity implements ISavable, ITriggerable, IEventFirable, IEditHandle
	{
		// Constants
		private static const TRIGGER_INPUTS:Vector.<String> = Vector.<String>( ["move"] );
		private static const TRIGGER_EVENTS:Vector.<String> = Vector.<String>( ["complete"] );
		private static const PROPERTIES:Vector.<String> = Vector.<String>( ["movedObject"] );
		private static const COMPLETE_EVENT:int = 0;
		private static const EDIT_HANDLE_COLOR:uint = 0x00ff00;
		private static const VERTEX_SIZE:int = 5;
		
		// Private variables
		private var editHandles:Boolean = false;
		private var placing:Boolean = false;
		
		// Public variables
		public var movedObject:String;
		public var _target:EventTargeter;
		
		// Objects
		private var points:Vector.<Point>;
		private var moveIndex:int;
		
		/**
		 * Create and initialize the movement path
		 * 
		 * @param	placing
		 * Whether or not the path is in edit mode
		 */
		public function Path( setMovedObject:String = "", setPlacing:Boolean = true ) 
		{
			// Set object properties
			movedObject = setMovedObject;
			placing = setPlacing;
			points = new Vector.<Point>();
			_target = new EventTargeter();
			setHitbox( VERTEX_SIZE, VERTEX_SIZE );
			
			// Show edit handles if the object is being placed
			if ( placing )
			{
				editHandles = true;
				active = true;
				GameWorld.currentInstance.captureClicks = false;
			}
			else
			{
				active = false;
			}
		}
		
		/**
		 * Handle edit mode
		 */
		override public function update():void 
		{
			if ( placing && Input.mousePressed )
			{
				var newPoint:Point = new Point( world.mouseX, world.mouseY );
				AddPoint( newPoint );
				
				// Continue editing if the control key is held down
				if ( !Input.check( Key.CONTROL ) )
				{
					active = false;
					placing = false;
					GameWorld.currentInstance.captureClicks = true;
				}
			}
			
			super.update();
		}
		
		/**
		 * Draw the path if needed
		 */
		override public function render():void 
		{
			if ( editHandles )
			{
				var lastPoint:Point = null;
				for each( var point:Point in points )
				{
					// Draw the line from the last point if needed
					if ( lastPoint != null )
						Draw.line( lastPoint.x, lastPoint.y, point.x, point.y, EDIT_HANDLE_COLOR );
					
					// Draw the square at the vertex
					Draw.rectPlus( point.x - (VERTEX_SIZE >> 1), point.y - (VERTEX_SIZE >> 1), VERTEX_SIZE, VERTEX_SIZE, EDIT_HANDLE_COLOR, 1, false );
					
					lastPoint = point;
				}
			}
			
			super.render();
		}
		
		/**
		 * Add a new point to the end of the movement path
		 * 
		 * @param	newPoint
		 * Position of the new point
		 */
		public function AddPoint( newPoint:Point ):void
		{
			if ( points.length == 0 )
			{
				x = newPoint.x;
				y = newPoint.y;
			}
			
			points.push( newPoint );
		}
		
		/**
		 * Trigger the object
		 */
		public function Trigger( event:String, value:int ):void
		{
			if ( points.length > 0 )
			{
				var targetObject:IMovable = world.getInstance( movedObject ) as IMovable;
				if ( targetObject != null )
				{
					targetObject.WarpTo( points[0] );
					moveIndex = 0;
					NextMove();
				}
			}
		}
		
		/**
		 * Move to the next point in the path if needed
		 */
		private function NextMove():void
		{
			if ( points.length > ++moveIndex )
			{
				var targetObject:IMovable = world.getInstance( movedObject ) as IMovable;
				if ( targetObject != null )
				{
					targetObject.MoveTo( points[moveIndex], NextMove );
				}
			}
			else
			{
				_target.FireEvent( TRIGGER_EVENTS[COMPLETE_EVENT], 1 );
			}
		}
		
		/**
		 * Show the object's edit handle
		 */
		public function ShowHandle():void
		{
			editHandles = true;
		}
		
		/**
		 * Hide the object's edit handle
		 */
		public function HideHandle():void
		{
			editHandles = false;
		}
		
		/**
		 * Return the button as an XML object for saving
		 * 
		 * @return
		 * Serialized elevator object
		 */
		public function GetSaveObject():XML
		{
			var node:XML = <path movedObject={movedObject} name={name} />;
			
			for each( var point:Point in points )
			{
				node.appendChild( <point x={point.x} y={point.y} /> );
			}
			
			if ( _target != null )
				_target.Serialize( node );
			
			return node;
		}
		
		/**
		 * Load button from an XML object
		 * 
		 * @param	buttonXML
		 * XML object containing button definition
		 * @return
		 * Newly created button object
		 */
		public static function Deserialize( pathXML:XML ):Path
		{
			var path:Path = new Path( pathXML.@movedObject, false );
			
			for each( var node:XML in pathXML.point )
			{
				path.AddPoint( new Point( node.@x, node.@y ) );
			}
			
			path._target = new EventTargeter( pathXML );
			if ( pathXML.@name != null )
				path.name = pathXML.@name;
			
			return path;
		}
		
		/**
		 * Get the array of properties
		 * 
		 * @return
		 * Array of property names
		 */
		public function GetProperties():Vector.<String>
		{
			return PROPERTIES;
		}
		
		/**
		 * Get list of input trigger events for editor
		 */
		public function get triggers():Vector.<String>
		{
			return TRIGGER_INPUTS;
		}
		
		/**
		 * Get list of trigger events for editor
		 */
		public function get events():Vector.<String>
		{
			return TRIGGER_EVENTS;
		}
		
		/**
		 * Get event targeter
		 */
		public function get target():EventTargeter
		{
			return _target;
		}
	}
}