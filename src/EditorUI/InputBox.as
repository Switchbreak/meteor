package EditorUI 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.tweens.misc.Alarm;
	import net.flashpunk.Tween;
	import net.flashpunk.utils.*;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.FP;
	
	/**
	 * UI control for text input
	 * 
	 * @author Switchbreak
	 */
	public class InputBox extends Graphiclist 
	{
		// Constants
		private static const BOX_MIN_WIDTH:int = 200;		// Width of entry box
		private static const BOX_BACK:uint = 0;				// Background color of the entry box
		private static const PADDING:int = 5;				// Size of UI element padding
		private static const SPACING:int = 10;				// Size of UI element vertical spacing
		private static const BLINK_RATE:Number = 0.25;		// Rate at which the cursor blinks, in blinks/sec
		private static const CURSOR_SPACING:int = -2;		// Spacing between text entry and cursor
		private static const ENTRY_LENGTH:int = 50;			// Maximum length allowed for text entry
		
		// Public variables
		public var enteredText:String;
		public var width:int;
		public var height:int;
		
		// Private variables
		private var blinkTimer:Number;
		private var editing:Boolean = false;
		private var callback:Function;
		
		// Graphics
		private var textBox:Text;
		private var cursor:Image;
		
		/**
		 *  Create and initialize the input box
		 */
		public function InputBox( inputText:String, setX:int, setY:int, setWidth:int, setCallback:Function = null ) 
		{
			// Set object values
			super();
			active = true;
			relative = false;
			enteredText = inputText;
			callback = setCallback;
			
			// Add text entry box
			textBox = new Text( " ", setX, setY );
			textBox.width = setWidth;
			textBox.relative = false;
			add( textBox );
			
			height = textBox.height;
			width = textBox.width;
			
			// Add cursor
			cursor = Image.createRect( textBox.textWidth, textBox.textHeight );
			cursor.relative = false;
			cursor.visible = false;
			add( cursor );
			
			// Populate textbox
			textBox.text = enteredText;
			cursor.x = textBox.x + textBox.textWidth + CURSOR_SPACING;
			cursor.y = textBox.y;
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			if ( editing )
			{
				// Detect if the user pressed enter
				if ( Input.pressed( Key.ENTER ) || Input.pressed( Key.NUMPAD_ENTER ) ||
					(Input.mousePressed &&
					(Input.mouseX < textBox.x || Input.mouseX > textBox.x + textBox.width ||
					Input.mouseY < textBox.y || Input.mouseY > textBox.y + textBox.height )) )
				{
					CompleteEdit();
				}
				else
				{
					// Count time until next blink toggle
					blinkTimer += FP.elapsed;
					if ( blinkTimer >= BLINK_RATE )
					{
						blinkTimer = 0.0;
						cursor.visible = !cursor.visible;
					}
					
					// Display entered text and reposition cursor as necessary
					if ( Input.keyString.length > ENTRY_LENGTH )
						Input.keyString = Input.keyString.substr( 0, ENTRY_LENGTH );
					
					if ( Input.keyString != enteredText )
					{
						enteredText = Input.keyString;
						textBox.text = enteredText;
						cursor.x = textBox.x + textBox.textWidth + CURSOR_SPACING;
						cursor.y = textBox.y;
						callback( enteredText );
					}
					
					width = textBox.width;
				}
			}
			else
			{
				// If not editing, check if the user clicked on the text box
				if ( Input.mousePressed )
				{
					if( Input.mouseX >= textBox.x && Input.mouseX <= textBox.x + textBox.width &&
						Input.mouseY >= textBox.y && Input.mouseY <= textBox.y + textBox.height )
					{
						editing = true;
						cursor.visible = true;
						blinkTimer = 0.0;
						
						// Clear input buffer before accepting input
						Input.clear();
						Input.keyString = enteredText;
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Stop editing and call back with value
		 */
		private function CompleteEdit():void
		{
			editing = false;
			cursor.visible = false;
		}
	}

}