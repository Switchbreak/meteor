package EditorUI 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	
	/**
	 * Graphic for showing event details in the properties pane
	 * 
	 * @author Switchbreak
	 */
	public class EventProperties extends Graphiclist 
	{
		// Embeds
		[Embed(source = "../../img/Add.png")] private static const ADD_ICON:Class;
		[Embed(source = "../../img/Delete.png")] private static const DELETE_ICON:Class;
		
		// Constants
		private static const LIST_ITEM_COLOR:uint = 0xFFFFFF;
		private static const ICON_WIDTH:int = 16;
		private static const PADDING:int = 10;
		
		// Public variables
		public var height:int;
		
		// Private variables
		private var eventName:String;
		private var targets:Vector.<String>;
		private var addCallback:Function;
		private var deleteCallback:Function;
		private var editCallback:Function;
		
		// Graphics
		private var eventLabel:Text;
		private var addTarget:Stamp;
		private var targetLabels:Vector.<Text>;
		private var targetDeleteButtons:Vector.<Stamp>;
		
		/**
		 * Create and initialize the event properties display
		 * 
		 * @param	setX
		 * Position of the event properties
		 * @param	setY
		 * Position of the event properties
		 * @param	setEventName
		 * Name of the event being displayed
		 * @param	setTargets
		 * List of targets set for the current event
		 * @param	setAddCallback
		 * Function to call when the add button is pressed
		 * @param	setDeleteCallback
		 * Function to call when the delete button is pressed
		 * @param	setEditCallback
		 * Function to call when the edit button is pressed
		 */
		public function EventProperties( setX:int, setY:int, setEventName:String, setTargets:Vector.<String>, setAddCallback:Function, setDeleteCallback:Function, setEditCallback:Function ) 
		{
			super();
			active = true;
			scrollX = 0;
			scrollY = 0;
			relative = false;
			height = setY;
			eventName = setEventName;
			targets = setTargets;
			addCallback = setAddCallback;
			deleteCallback = setDeleteCallback;
			editCallback = setEditCallback;
			
			// Create event label graphic
			eventLabel = new Text( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "EVENT_LABEL" ), eventName ), setX, height );
			eventLabel.relative = false;
			add( eventLabel );
			
			// Create add button to add targets to the event
			addTarget = new Stamp( ADD_ICON, FP.width - ICON_WIDTH, height );
			addTarget.relative = false;
			add( addTarget );
			
			height += eventLabel.height;
			
			// Show list of targets
			targetLabels = new Vector.<Text>();
			targetDeleteButtons = new Vector.<Stamp>();
			
			for each( var eventTarget:String in targets )
			{
				// Create the event target label graphic
				var targetLabel:Text = new Text( StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "TARGET_LABEL" ), eventTarget ), setX, height );
				targetLabel.relative = false;
				targetLabels.push( add( targetLabel ) as Text);
				
				// Create event target delete button
				var targetDeleteIcon:Stamp = new Stamp( DELETE_ICON, FP.width - ICON_WIDTH - PADDING, height );
				targetDeleteIcon.relative = false;
				targetDeleteButtons.push( add( targetDeleteIcon ) as Stamp );
				
				height += targetLabel.height;
			}
			
			height -= setY;
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Handle clicks
			if ( Input.mousePressed )
			{
				// Check if the user clicked on a delete button
				for( var i:int = 0; i < targetDeleteButtons.length; ++i )
				{
					if ( Input.mouseX >= targetDeleteButtons[i].x
						&& Input.mouseX <= targetDeleteButtons[i].x + targetDeleteButtons[i].width
						&& Input.mouseY >= targetDeleteButtons[i].y
						&& Input.mouseY <= targetDeleteButtons[i].y + targetDeleteButtons[i].height )
					{
						deleteCallback( eventName, i );
						break;
					}
				}
				
				// Check if the user clicked on a label
				for( i = 0; i < targetLabels.length; ++i )
				{
					if ( Input.mouseX >= targetLabels[i].x
						&& Input.mouseX <= targetLabels[i].x + targetLabels[i].width
						&& Input.mouseY >= targetLabels[i].y
						&& Input.mouseY <= targetLabels[i].y + targetLabels[i].height )
					{
						editCallback( eventName, i );
						break;
					}
				}
				
				// Check if the user clicked on an add button
				if ( Input.mouseX >= addTarget.x
					&& Input.mouseX <= addTarget.x + addTarget.width
					&& Input.mouseY >= addTarget.y
					&& Input.mouseY <= addTarget.y + addTarget.height )
				{
					addCallback( eventName );
				}
			}
			
			super.update();
		}
		
	}

}