package EditorUI 
{
	import flash.utils.ByteArray;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Text;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	import net.flashpunk.utils.Input;
	
	/**
	 * UI object to show a list of image layers in the current scene for
	 * editing, reordering or removing.
	 * 
	 * @author Switchbreak
	 */
	public class ImageLayerList extends Entity 
	{
		// Embeds
		[Embed(source = "../../img/ArrowDown.png")] private static const ARROW_DOWN:Class;
		[Embed(source = "../../img/ArrowUp.png")] private static const ARROW_UP:Class;
		[Embed(source = "../../img/Delete.png")] private static const DELETE:Class;
		
		// Constants
		private static const BACKDROP_WIDTH:int = 175;
		private static const BACKDROP_COLOR:uint = 0;
		private static const ICON_WIDTH:int = 17;
		private static const LIST_ITEM_COLOR:uint = 0xFFFFFF;
		private static const HIDDEN_COLOR:uint = 0x888888;
		private static const PADDING:int = 10;
		private static const ICON_COUNT:int = 3;
		
		// Private variables
		private var imageLayers:Vector.<Entity>;
		
		// Graphics
		private var backdrop:Image;
		private var heading:Text;
		private var imageLabels:Vector.<Text>;
		private var icons:Vector.<Stamp>;
		
		/**
		 * Create and initialize the image layer list object
		 */
		public function ImageLayerList() 
		{
			// Set object values
			layer = GameWorld.HUD_LAYER;
			super();
			
			// Create backdrop
			backdrop = Image.createRect( BACKDROP_WIDTH, FP.height, BACKDROP_COLOR );
			backdrop.x = FP.width - BACKDROP_WIDTH;
			backdrop.scrollX = 0;
			backdrop.scrollY = 0;
			backdrop.relative = false;
			addGraphic( backdrop );
			
			// Create heading
			heading = new Text( ResourceManager.getInstance().getString( "resources", "IMAGE_LAYER_LIST_HEADER" ), backdrop.x + PADDING, PADDING );
			heading.scrollX = 0;
			heading.scrollY = 0;
			heading.relative = false;
			addGraphic( heading );
			
			// Get all image layers on camera
			PopulateImageLayers();
			CreateImageLayerGraphics();
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			if ( Input.mousePressed )
			{
				// Capture clicks on any label in the image layer list
				for ( var i:int = 0; i < imageLayers.length; ++i )
				{
					if ( Input.mouseX >= imageLabels[i].x
						&& Input.mouseX <= imageLabels[i].x + imageLabels[i].width
						&& Input.mouseY >= imageLabels[i].y
						&& Input.mouseY <= imageLabels[i].y + imageLabels[i].height )
					{
						imageLayers[i].visible = !imageLayers[i].visible;
						imageLabels[i].color = (imageLayers[i].visible ? LIST_ITEM_COLOR : HIDDEN_COLOR);
					}
				}
				
				// Capture clicks on the icons
				for ( i = 0; i < icons.length; ++i )
				{
					if ( Input.mouseX >= icons[i].x
						&& Input.mouseX <= icons[i].x + icons[i].width
						&& Input.mouseY >= icons[i].y
						&& Input.mouseY <= icons[i].y + icons[i].height )
					{
						var imageIndex:int = i / ICON_COUNT;
						var button:int = i % ICON_COUNT;
						
						switch( button )
						{
							case 0:
								// Up arrow was clicked
								if ( imageIndex > 0 )
								{
									SwapImageLayers( imageLayers[imageIndex], imageLayers[imageIndex - 1] );
								}
								break;
							case 1:
								// Down arrow was clicked
								if ( imageIndex < imageLayers.length - 1 )
								{
									SwapImageLayers( imageLayers[imageIndex], imageLayers[imageIndex + 1] );
								}
								break;
							case 2:
								// Delete button was clicked
								DeleteImageLayer( imageLayers[imageIndex] );
								break;
						}
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Get all image layers that are on camera and put them in imageLayers
		 */
		private function PopulateImageLayers():void
		{
			// Clear the image layer list
			imageLayers = new Vector.<Entity>();
			
			// Check every image layer in the world and see if it is on camera
			for each( var imageLayer:Entity in GameWorld.currentInstance.imageLayers )
			{
				if ( imageLayer.onCamera
					&& imageLayer.x < FP.camera.x + FP.width
					&& imageLayer.x + imageLayer.width > FP.camera.x
					&& imageLayer.y < FP.camera.y + FP.height
					&& imageLayer.y + imageLayer.height > FP.camera.y )
				{
					imageLayers.push( imageLayer );
				}
			}
		}
		
		/**
		 * Create the graphics objects for the image layer list
		 */
		private function CreateImageLayerGraphics():void
		{
			// Clear the old graphics if necessary
			if ( imageLabels != null )
			{
				for each( var imageLabel:Text in imageLabels )
					(graphic as Graphiclist).remove( imageLabel );
			}
			if ( icons != null )
			{
				for each( var icon:Stamp in icons )
					(graphic as Graphiclist).remove( icon );
			}
			
			// Show list of image layers
			imageLabels = new Vector.<Text>();
			icons = new Vector.<Stamp>();
			var imageIndex:int = 1;
			var labelY:int = heading.y + heading.height + PADDING;
			for each( var imageLayer:Entity in imageLayers )
			{
				var layerName:String;
				
				// Get image name or generate name if it isn't set
				if ( imageLayer.name == null || imageLayer.name == "" )
					imageLayer.name = StringUtil.substitute( ResourceManager.getInstance().getString( "resources", "IMAGE_LAYER_NAME" ), imageIndex++ );
				
				layerName = imageLayer.name;
				
				// Create image label graphic
				imageLabel = new Text( layerName, backdrop.x + PADDING, labelY );
				imageLabel.scrollX = 0;
				imageLabel.scrollY = 0;
				imageLabel.relative = false;
				imageLabel.color = (imageLayer.visible ? LIST_ITEM_COLOR : HIDDEN_COLOR);
				addGraphic( imageLabel );
				imageLabels.push( imageLabel );
				
				// Create the icons
				var iconX:int = FP.width - (ICON_WIDTH * ICON_COUNT);
				
				icon = new Stamp( ARROW_UP, iconX, labelY );
				icon.relative = false;
				icon.scrollX = 0;
				icon.scrollY = 0;
				icons.push( addGraphic( icon ) as Stamp );
				iconX += ICON_WIDTH;
				
				icon = new Stamp( ARROW_DOWN, iconX, labelY );
				icon.relative = false;
				icon.scrollX = 0;
				icon.scrollY = 0;
				icons.push( addGraphic( icon ) as Stamp );
				iconX += ICON_WIDTH;
				
				icon = new Stamp( DELETE, iconX, labelY );
				icon.relative = false;
				icon.scrollX = 0;
				icon.scrollY = 0;
				icons.push( addGraphic( icon ) as Stamp );
				
				// Move vertical list cursor down
				labelY += imageLabel.height;
			}
		}
		
		/**
		 * Swap the position of two image layers in the layer's draw order and
		 * in the imageLayers vector.
		 * 
		 * @param	image1
		 * First image layer to be swapped
		 * @param	image2
		 * Second image layer to be swapped
		 */
		private function SwapImageLayers( image1:Entity, image2:Entity ):void
		{
			world.swapPosition( image1, image2 );
			
			var index1:int = GameWorld.currentInstance.imageLayers.indexOf( image1 );
			var index2:int = GameWorld.currentInstance.imageLayers.indexOf( image2 );
			GameWorld.currentInstance.imageLayers[index1] = image2;
			GameWorld.currentInstance.imageLayers[index2] = image1;
			
			var swap:ByteArray = GameWorld.currentInstance.layerSources[index1];
			GameWorld.currentInstance.layerSources[index1] = GameWorld.currentInstance.layerSources[index2];
			GameWorld.currentInstance.layerSources[index2] = swap;
			
			PopulateImageLayers();
			CreateImageLayerGraphics();
		}
		
		/**
		 * Remove an image layer from the world
		 * 
		 * @param	imageLayer
		 * Image to remove
		 */
		private function DeleteImageLayer( imageLayer:Entity ):void
		{
			world.remove( imageLayer );
			GameWorld.currentInstance.imageLayers.splice( GameWorld.currentInstance.imageLayers.indexOf( imageLayer ), 1 );
			GameWorld.currentInstance.layerSources.splice( GameWorld.currentInstance.imageLayers.indexOf( imageLayer ), 1 );
			
			PopulateImageLayers();
			CreateImageLayerGraphics();
		}
		
	}

}