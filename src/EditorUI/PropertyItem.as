package EditorUI 
{
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	
	/**
	 * Graphic for showing event details in the properties pane
	 * 
	 * @author Switchbreak
	 */
	public class PropertyItem extends Graphiclist 
	{
		// Constants
		private static const LIST_ITEM_COLOR:uint = 0xFFFFFF;
		private static const ICON_WIDTH:int = 16;
		private static const PADDING:int = 10;
		
		// Public variables
		public var height:int;
		
		// Private variables
		private var propertyName:String;
		private var propertyValue:String;
		private var editCallback:Function;
		
		// Graphics
		private var propertyLabel:Text;
		private var propertyInput:InputBox;
		
		/**
		 * Create and initialize the property display item
		 * 
		 * @param	setX
		 * Position of the event properties
		 * @param	setY
		 * Position of the event properties
		 * @param	setPropertyName
		 * Name of the property to display
		 * @param	setEditCallback
		 * Function to call when the edit button is pressed
		 */
		public function PropertyItem( setX:int, setY:int, setPropertyName:String, setPropertyValue:String, setEditCallback:Function ) 
		{
			super();
			active = true;
			scrollX = 0;
			scrollY = 0;
			relative = false;
			height = setY;
			propertyName = setPropertyName;
			propertyValue = setPropertyValue;
			editCallback = setEditCallback;
			
			// Create event label graphic
			propertyLabel = new Text( propertyName, setX, setY );
			propertyLabel.relative = false;
			add( propertyLabel );
			height += propertyLabel.height;
			
			// Create input graphic
			propertyInput = new InputBox( propertyValue, setX, height, FP.width - setX - PADDING, EditCallback );
			add( propertyInput );
			height += propertyInput.height;
			
			height -= setY;
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Handle clicks
			if ( Input.mousePressed )
			{
				// TODO: Handle edit properties
			}
			
			super.update();
		}
		
		/**
		 * Sets the property value and passes it to the callback
		 * 
		 * @param	setPropertyValue
		 * Value to set
		 */
		private function EditCallback( setPropertyValue:String ):void
		{
			propertyValue = setPropertyValue;
			editCallback( propertyName, propertyValue );
		}
		
	}

}