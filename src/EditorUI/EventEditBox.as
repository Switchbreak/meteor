package EditorUI 
{
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import net.flashpunk.utils.Input;
	import mx.resources.ResourceManager;
	
	/**
	 * A dialogue box for editing an event target
	 * 
	 * @author Switchbreak
	 */
	public class EventEditBox extends Graphiclist 
	{
		// Constants
		private static const SPACING:int = 10;
		private static const BACK_COLOR:uint = 0;
		private static const BOX_WIDTH:int = 350;
		private static const BOX_HEIGHT:int = 100;
		
		// Private variables
		private var eventName:String;
		private var targetName:String;
		private var targetEvent:String;
		private var targetIndex:int;
		private var callback:Function;
		
		// Graphics
		private var targetInput:InputBox;
		private var targetEventInput:InputBox;
		private var OKButton:Text;
		
		/**
		 * Create the dialog box
		 */
		public function EventEditBox( setEventName:String, setTargetName:String, setTargetEvent:String, setCallback:Function, setTargetIndex:int = -1 ) 
		{
			// Set object values
			x = FP.halfWidth - (BOX_WIDTH >> 1);
			y = FP.halfHeight - (BOX_HEIGHT >> 1);
			active = true;
			relative = false;
			scrollX = 0;
			scrollY = 0;
			eventName = setEventName;
			targetName = setTargetName;
			targetEvent = setTargetEvent;
			callback = setCallback;
			targetIndex = setTargetIndex;
			
			// Create target input
			var labelY:int = SPACING;
			
			var targetLabel:Text = new Text( ResourceManager.getInstance().getString( "resources", "TARGET_NAME" ), SPACING, labelY );
			labelY += targetLabel.height;
			
			targetInput = new InputBox( targetName, x + SPACING, y + labelY, BOX_WIDTH - (SPACING << 1), SetTargetName );
			labelY += targetInput.height;
			
			var targetEventLabel:Text = new Text( ResourceManager.getInstance().getString( "resources", "TARGET_EVENT_NAME" ), SPACING, labelY );
			labelY += targetEventLabel.height;
			
			targetEventInput = new InputBox( targetEvent, x + SPACING, y + labelY, BOX_WIDTH - (SPACING << 1), SetTargetEvent );
			labelY += targetEventInput.height;
			
			// Create OK button 
			OKButton = new Text( "OK" );
			OKButton.x = x + BOX_WIDTH - OKButton.width - SPACING;
			OKButton.y = y + labelY;
			OKButton.relative = false;
			labelY += OKButton.height;
			
			// Create background
			add( Image.createRect( BOX_WIDTH, labelY + SPACING, BACK_COLOR ) );
			
			// Add graphics
			add( targetLabel );
			add( targetInput );
			add( targetEventLabel );
			add( targetEventInput );
			add( OKButton );
		}
		
		/**
		 * Process input
		 */
		override public function update():void 
		{
			// Detect clicks on the OK button
			if ( Input.mousePressed &&
				Input.mouseX >= OKButton.x && Input.mouseX <= OKButton.x + OKButton.width &&
				Input.mouseY >= OKButton.y && Input.mouseY <= OKButton.y + OKButton.height )
			{
				if( targetIndex > -1 )
					callback( eventName, targetIndex, targetName, targetEvent );
				else
					callback( eventName, targetName, targetEvent );
			}
			
			super.update();
		}
		
		/**
		 * Sets the target name when the input box is entered
		 * 
		 * @param	setTargetName
		 * Value of the target name
		 */
		private function SetTargetName( setTargetName:String ):void
		{
			targetName = setTargetName;
		}
		
		/**
		 * Sets the target event name when the input box is entered
		 * 
		 * @param	setTargetEvent
		 * Value of the target event name
		 */
		private function SetTargetEvent( setTargetEvent:String ):void
		{
			targetEvent = setTargetEvent;
		}
	}
}