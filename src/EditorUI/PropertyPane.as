package EditorUI 
{
	import flash.utils.Dictionary;
	import Interfaces.IEventFirable;
	import Interfaces.ISavable;
	import net.flashpunk.Entity;
	import net.flashpunk.graphics.Graphiclist;
	import net.flashpunk.graphics.Image;
	import net.flashpunk.graphics.Stamp;
	import net.flashpunk.graphics.Text;
	import net.flashpunk.FP;
	import mx.resources.ResourceManager;
	import mx.utils.StringUtil;
	import net.flashpunk.utils.Input;
	
	/**
	 * ...
	 * @author Switchbreak
	 */
	public class PropertyPane extends Entity 
	{
		// Constants
		private static const BACKDROP_WIDTH:int = 175;
		private static const BACKDROP_COLOR:uint = 0;
		private static const LIST_ITEM_COLOR:uint = 0xFFFFFF;
		private static const PADDING:int = 10;
		
		// Private variables
		private var eventTop:int;
		
		// Graphics
		private var backdrop:Image;
		private var heading:InputBox;
		private var properties:Vector.<PropertyItem>;
		private var events:Vector.<EventProperties>;
		private var editBox:EventEditBox;
		
		// Objects
		private var target:Entity;
		
		/**
		 * Create and initialize the property pane
		 * 
		 * @param	targetEntity
		 * The entity whose properties are being displayed
		 */
		public function PropertyPane( targetEntity:Entity ) 
		{
			// Set object values
			layer = GameWorld.HUD_LAYER;
			target = targetEntity;
			super( FP.width - BACKDROP_WIDTH );
			
			var targetName:String = targetEntity.name;
			if ( targetName == null || targetName == "" )
				targetName = targetEntity.toString();
			
			// Create backdrop
			backdrop = Image.createRect( BACKDROP_WIDTH, FP.height, BACKDROP_COLOR );
			backdrop.x = x;
			backdrop.scrollX = 0;
			backdrop.scrollY = 0;
			backdrop.relative = false;
			addGraphic( backdrop );
			
			// Create heading
			heading = new InputBox( targetName, x + PADDING, PADDING, BACKDROP_WIDTH - (PADDING << 1), SetName );
			heading.scrollX = 0;
			heading.scrollY = 0;
			heading.relative = false;
			addGraphic( heading );
			
			// Disable world object's input
			GameWorld.currentInstance.showHandles = false;
			GameWorld.currentInstance.captureKeys = false;
			
			PopulatePropertyList();
			PopulateEventList();
		}
		
		/**
		 * Reenable input on removal from the world
		 */
		override public function removed():void 
		{
			GameWorld.currentInstance.captureKeys = true;
			GameWorld.currentInstance.captureClicks = true;
			GameWorld.currentInstance.showHandles = true;
			super.removed();
		}
		
		/**
		 * Get events from target entity and show them
		 */
		private function PopulatePropertyList():void
		{
			// Clear existing event list if needed
			if ( properties != null )
			{
				for each( var property:PropertyItem in properties )
				{
					(graphic as Graphiclist).remove( property );
				}
			}
			
			properties = new Vector.<PropertyItem>();
			
			// Get list of events from the target entity
			var propertyNames:Vector.<String> = (target as ISavable).GetProperties();
			
			var propertyY:int = heading.y + heading.height + (PADDING << 1);
			
			// Create list item for each event
			for each( var propertyName:String in propertyNames )
			{
				// Get property value
				var propertyValue:String = target[propertyName].toString();
				
				property = new PropertyItem( x + PADDING, propertyY, propertyName, propertyValue, SetPropertyValue );
				properties.push( addGraphic( property ) as PropertyItem );
				
				propertyY += property.height;
			}
			
			eventTop = propertyY + (PADDING << 1);
		}
		
		/**
		 * Get events from target entity and show them
		 */
		private function PopulateEventList():void
		{
			// Clear existing event list if needed
			if ( events != null )
			{
				for each( var event:EventProperties in events )
				{
					(graphic as Graphiclist).remove( event );
				}
			}
			
			events = new Vector.<EventProperties>();
			
			// If the object has events, list them
			if ( target is IEventFirable )
			{
				// Get list of events from the target entity
				var eventNames:Vector.<String> = (target as IEventFirable).events;
				
				var eventY:int = eventTop;
				
				// Create list item for each event
				for each( var eventName:String in eventNames )
				{
					var targetNames:Vector.<String> = (target as IEventFirable).target.GetTargetList( eventName );
					
					event = new EventProperties( x + PADDING, eventY, eventName, targetNames, AddTarget, DeleteTarget, EditTarget );
					events.push( addGraphic( event ) as EventProperties );
					
					eventY += event.height;
				}
			}
		}
		
		/**
		 * Sets the value of a property on the target entity
		 * 
		 * @param	propertyName
		 * Name of the property to set
		 * @param	propertyValue
		 * Value to set the property to
		 */
		private function SetPropertyValue( propertyName:String, propertyValue:String ):void
		{
			if ( target[propertyName] is Boolean )
			{
				if ( propertyValue == "true" )
					target[propertyName] = true;
				else if ( propertyValue == "false" )
					target[propertyName] = false;
			}
			else
			{
				target[propertyName] = propertyValue;
			}
		}
		
		/**
		 * Callback method called by EventProperty when delete is pressed
		 * 
		 * @param	eventName
		 * Name of the event containing the target to be deleted
		 * @param	targetIndex
		 * Index of the target to delete
		 */
		private function DeleteTarget( eventName:String, targetIndex:int ):void
		{
			(target as IEventFirable).target.DeleteTarget( eventName, targetIndex );
			PopulateEventList();
		}
		
		/**
		 * Callback method called by EventProperty when add is pressed
		 * 
		 * @param	eventName
		 * Name of the event containing the target to be deleted
		 */
		private function AddTarget( eventName:String ):void
		{
			editBox = new EventEditBox( eventName, "", "", CommitAddTarget );
			addGraphic( editBox );
			GameWorld.currentInstance.captureClicks = false;
		}
		
		/**
		 * Method to add a new target after the edit box is complete
		 * 
		 * @param	eventName
		 * Name of the event to add the new target on
		 * @param	targetName
		 * Name of the new target
		 * @param	targetEvent
		 * Name of the new target event
		 */
		private function CommitAddTarget( eventName:String, targetName:String, targetEvent:String ):void
		{
			(graphic as Graphiclist).remove( editBox );
			GameWorld.currentInstance.captureClicks = true;
			
			(target as IEventFirable).target.AddTarget( eventName, targetName, targetEvent );
			PopulateEventList();
		}
		
		/**
		 * Callback method called by EventProperty when add is pressed
		 * 
		 * @param	eventName
		 * Name of the event containing the target to be deleted
		 */
		private function EditTarget( eventName:String, targetIndex:int ):void
		{
			var targetEvent:Vector.<String> = (target as IEventFirable).target.ReturnTarget( eventName, targetIndex );
			
			editBox = new EventEditBox( eventName, targetEvent[0], targetEvent[1], CommitEditTarget, targetIndex );
			addGraphic( editBox );
			GameWorld.currentInstance.captureClicks = false;
		}
		
		/**
		 * Method to add a new target after the edit box is complete
		 * 
		 * @param	eventName
		 * Name of the event to add the new target on
		 * @param	targetName
		 * Name of the new target
		 * @param	targetEvent
		 * Name of the new target event
		 */
		private function CommitEditTarget( eventName:String, targetIndex:int, targetName:String, targetEvent:String ):void
		{
			(graphic as Graphiclist).remove( editBox );
			GameWorld.currentInstance.captureClicks = true;
			
			(target as IEventFirable).target.EditTarget( eventName, targetIndex, targetName, targetEvent );
			PopulateEventList();
		}
		
		/**
		 * Sets the name of the game object
		 * 
		 * @param	objectName
		 * Name to set
		 */
		private function SetName( objectName:String ):void
		{
			target.name = objectName;
		}
	}

}