package 
{
	import net.flashpunk.Engine;
	import net.flashpunk.FP;
	import net.flashpunk.World;
	
	/**
	 * Main class to initialize engine and game world object
	 * 
	 * @author Switchbreak
	 */
	public class Main extends Engine 
	{
		
		/**
		 * Application entry point
		 */
		public function Main():void 
		{
			super( 640, 480 );
		}
		
		/**
		 * Function called after engine finishes initializing to create game
		 * world object
		 */
		override public function init():void 
		{
			FP.world = new TitleScreen();
		}
		
	}
	
}