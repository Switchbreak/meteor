package  
{
	import net.flashpunk.Entity;
	import net.flashpunk.FP;
	import net.flashpunk.graphics.Image;
	import flash.geom.Point;
	import net.flashpunk.utils.*;
	
	/**
	 * Collision line editor
	 * 
	 * @author Switchbreak
	 */
	public class DrawLines extends Entity 
	{
		// Embeds
		[Embed(source = "../img/Arrow.png")] private static const ARROW:Class;
		
		// Constants
		private static const SNAP_RADIUS:int = 5;
		private static const SNAP_RADIUS_SQUARED:int = SNAP_RADIUS * SNAP_RADIUS;
		private static const SNAP_MARKER_RADIUS:int = 3;
		private static const SNAP_COLOR:uint = 0xFFFFFF;
		
		// Private variables
		private static var snapEnabled:Boolean = true;
		private var flipped:Boolean = false;
		
		// Editor objects
		private var newLine:Line = null;
		private var snap1:Image;
		private var snap2:Image;
		private var arrowSprite:Image;
		private var callback:Function;
		private var danger:Boolean;
		
		/**
		 * Create and initialize the line editor
		 */
		public function DrawLines( setCallback:Function = null, setDanger:Boolean = false ) 
		{
			// Set object values
			layer = GameWorld.HUD_LAYER;
			callback = setCallback;
			danger = setDanger;
			
			// Create snap graphics
			snap1 = Image.createCircle( SNAP_MARKER_RADIUS, SNAP_COLOR );
			snap1.centerOrigin();
			snap1.visible = false;
			addGraphic( snap1 );
			snap2 = Image.createCircle( SNAP_MARKER_RADIUS, SNAP_COLOR );
			snap2.centerOrigin();
			snap2.visible = false;
			addGraphic( snap2 );
			
			// Create arrow graphic
			arrowSprite = new Image( ARROW );
			arrowSprite.visible = false;
			arrowSprite.originX = arrowSprite.width >> 1;
			arrowSprite.originY = arrowSprite.height;
			addGraphic( arrowSprite );
			
			GameWorld.currentInstance.showHandles = true;
		}
		
		/**
		 * Handle input
		 */
		override public function update():void 
		{
			// Keyboard input
			if ( Input.released( Key.S ) )
			{
				snapEnabled = !snapEnabled;
			}
			if ( Input.released( Key.F ) )
			{
				flipped = !flipped;
			}
			
			//
			// Mouse input
			//
			
			// If a line is being created, have the endpoint follow the mouse
			if ( newLine )
			{
				var mousePoint:Point = new Point( world.mouseX, world.mouseY );
				if ( snapEnabled )
				{
					if ( SnapToLines( mousePoint ) && !mousePoint.equals( newLine.end1 ) )
					{
						snap2.x = mousePoint.x;
						snap2.y = mousePoint.y;
						snap2.visible = true;
					}
					else
					{
						snap2.visible = false;
					}
				}
				
				newLine.end2 = mousePoint;
				arrowSprite.x = (newLine.end1.x + newLine.end2.x) >> 1;
				arrowSprite.y = (newLine.end1.y + newLine.end2.y) >> 1;
				arrowSprite.angle = FP.angle( newLine.end1.x, newLine.end1.y, newLine.end2.x, newLine.end2.y ) + (flipped ? 180 : 0);
			}
			
			// Create lines when the user clicks on the screen
			if ( Input.mouseReleased )
			{
				snap1.visible = false;
				snap2.visible = false;
				
				if ( newLine )
				{
					if ( flipped )
					{
						var end2:Point = newLine.end2;
						newLine.end2 = newLine.end1;
						newLine.end1 = end2;
					}
					
					if ( callback != null )
					{
						callback( newLine );
						world.remove( this );
						return;
					}
				}
				
				// Create a new line if there is not a line currently being
				// drawn. If there is one, end the line or create a new one
				// immediately if CTRL is held down.
				if ( !newLine || Input.check( Key.CONTROL ) )
				{
					mousePoint = new Point( world.mouseX, world.mouseY );
					
					if ( snapEnabled )
					{
						if ( SnapToLines( mousePoint ) && (!newLine || Input.check( Key.CONTROL )) )
						{
							snap1.x = mousePoint.x;
							snap1.y = mousePoint.y;
							snap1.visible = true;
						}
					}
					
					newLine = new Line( mousePoint, mousePoint );
					newLine.danger = danger;
					GameWorld.currentInstance.collisionLines.push( newLine );
					
					arrowSprite.x = mousePoint.x;
					arrowSprite.y = mousePoint.y;
					arrowSprite.visible = true;
					Input.mouseCursor = "hide";
				}
				else
				{
					newLine = null;
					arrowSprite.visible = false;
					Input.mouseCursor = "auto";
				}
			}
			else if ( Input.mouseDown && !newLine )
			{
				if ( snapEnabled )
				{
					mousePoint = new Point( world.mouseX, world.mouseY );
					if ( SnapToLines( mousePoint ) )
					{
						snap1.x = mousePoint.x;
						snap1.y = mousePoint.y;
						snap1.visible = true;
					}
					else
					{
						snap1.visible = false;
					}
				}
			}
			
			super.update();
		}
		
		/**
		 * Clean up after entity is removed
		 */
		override public function removed():void 
		{
			// Remove newline if it exists
			if ( newLine )
			{
				GameWorld.currentInstance.collisionLines.splice( GameWorld.currentInstance.collisionLines.indexOf( newLine ), 1 );
				Input.mouseCursor = "auto";
			}
			
			super.removed();
		}
		
		/**
		 * Snap a point to a near line endpoint if applicable
		 * 
		 * @param	x
		 * Position of the point to snap
		 * @param	y
		 * Position of the point to snap
		 */
		private function SnapToLines( snapPoint:Point ):Boolean
		{
			// Get all lines
			var lines:Vector.<Line> = GameWorld.currentInstance.collisionLines;
			
			// Check all line endpoints
			for each( var line:Line in lines )
			{
				if ( line != newLine )
				{
					if ( line.DistanceToLine( snapPoint, SNAP_RADIUS_SQUARED ) )
						return true;
				}
			}
			
			var retVal:Boolean = false;
			
			// Snap to edge of screen
			if ( snapPoint.x - world.camera.x < SNAP_RADIUS )
			{
				snapPoint.x = world.camera.x;
				retVal = true;
			}
			else if ( snapPoint.x - world.camera.x > FP.width - SNAP_RADIUS )
			{
				snapPoint.x = FP.width + world.camera.x;
				retVal = true;
			}
			if ( snapPoint.y - world.camera.y < SNAP_RADIUS )
			{
				snapPoint.y = world.camera.y;
				retVal = true;
			}
			else if ( snapPoint.y - world.camera.y > FP.height - SNAP_RADIUS )
			{
				snapPoint.y = FP.height + world.camera.y;
				retVal = true;
			}
			
			return retVal;
		}
	}

}