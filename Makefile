MXMLC = /Users/lsd/flex_sdk/bin/mxmlc
#FLIXEL = /home/lsd/projects/flash-test/ChevyRay-FlashPunk-46c75be
SRC = src/* 
MAIN = src/Main.as
SWF = bin/Meteor.swf
 
$(SWF) : $(SRC)
	$(MXMLC) -sp src -sp FlashPunk -static-link-runtime-shared-libraries=true -locale=en_US -source-path+=locale/en_US -debug=true -o $(SWF) -- $(MAIN)

clean :
	rm $(SWF)
